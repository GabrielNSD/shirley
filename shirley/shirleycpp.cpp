
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <escript/AbstractDomain.h>

#include <escript/ExceptionTranslators.h>
#include <escript/SubWorld.h>

#include <shirley/ShirleyException.h>
#include <shirley/ShirleyDomain.h>

#include <shirley/LobattoPolynomials.h>
#include <shirley/VdmSystemsTri3.h>

#include <shirley/Ricker.h>

#include <boost/python.hpp>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/version.hpp>

#include <boost/detail/endian.hpp>  // For BOOST_BYTE_ORDER

using namespace boost::python;

namespace shirley {

// BYTEORDER_* and DATATYPE_* definitions copied from
// include/speckley/system_dep.h.
enum {
    BYTEORDER_NATIVE = BOOST_BYTE_ORDER,
    BYTEORDER_LITTLE_ENDIAN = 1234,
    BYTEORDER_BIG_ENDIAN = 4321
};

enum {
    DATATYPE_INT32 = 1,
    DATATYPE_FLOAT32,
    DATATYPE_FLOAT64
};

int _checkShirleyVersion()
{
    return 20211020;
}

escript::Domain_ptr _readGmsh(
    const std::string & filename, int numDim, int order, bool optimize,
    const boost::python::api::object& objPoints,
    const boost::python::api::object& objTags,
    int traversalStrategy)
{
    boost::python::list pyPoints = extract<boost::python::list>(objPoints);
    boost::python::list pyTags = extract<boost::python::list>(objTags);
    int numPts = extract<int>(pyPoints.attr("__len__")());
    int numTags = extract<int>(pyTags.attr("__len__")());
    std::vector<double> points;
    std::vector<int> tags;
    tags.resize(numTags, -1);

    for (int i = 0; i < numPts; ++i) {
        tuple temp = extract<tuple>(pyPoints[i]);
        int l = extract<int>(temp.attr("__len__")());

        if (l != 2) {
            throw ShirleyException(
                "shirley::_readGmsh(): Number of coordinates for "
                "each Dirac point must match dimensions.");
        }

        for (int k = 0; k < l; ++k) {
            points.push_back(extract<double>(temp[k]));
        }
    }

    TagMap tagsToNames;
    int curMax = 40;
    // But which order to assign tags to names?????
    for (int i = 0; i < numTags; ++i) {
        extract<int> ex_int(pyTags[i]);
        extract<std::string> ex_str(pyTags[i]);

        if (ex_int.check()) {
            tags[i] = ex_int();

            if (tags[i] >= curMax) {
                curMax = tags[i] + 1;
            }
        } else if (ex_str.check()) {
            std::string s = ex_str();
            auto it = tagsToNames.find(s);

            if (it != tagsToNames.end()) {
                // We have the tag already so look it up
                tags[i] = it->second;
            } else {
                tagsToNames[s] = curMax;
                tags[i] = curMax;
                curMax++;
            }
        } else {
            throw ShirleyException(
                "shirley::_readGmsh(): Error - Unable to extract tag value.");
        }
    }

    if (numTags != numPts) {
        throw ShirleyException(
            "shirley::_readGmsh(): Number of tags does not match number of points.");
    }

    escript::JMPI mpiInfo = escript::makeInfo(MPI_COMM_WORLD);
    escript::Domain_ptr result = shirley::ShirleyDomain::readGmsh(
        mpiInfo, filename, numDim, order, optimize,
        traversalStrategy);

    // Adds Dirac points and tags.
    auto* domain = (shirley::ShirleyDomain *) result.get();

    for (auto i = tagsToNames.begin(); i != tagsToNames.end(); i++) {
        domain->setTagMap(i->first, i->second);
    }

    domain->addPoints(points, tags);

    return result;
}

Ricker _ricker(double f_dom, double t_dom)
{
    return Ricker(f_dom, t_dom);
}

void _printDataInternals(/*const*/ escript::Data& data)
{
    std::cout << "printDataInternals()" << std::endl;

    unsigned int rank = data.getDataPointRank();

    std::cout << "rank: " << rank << std::endl;
    std::cout << "shape: [ ";

    const escript::DataTypes::ShapeType &dataShape = data.getDataPointShape();

    for (auto iter = dataShape.begin(); iter != dataShape.end(); ++iter) {
        std::cout << *iter << " ";
    }

    std::cout << "] " << std::endl;
    std::cout << "samples: " << data.getNumSamples() << std::endl;
    std::cout << "data points per sample: " << data.getNumDataPointsPerSample() << std::endl;
    std::cout << "data point size: " << data.getDataPointSize() << std::endl;
    std::cout << "values (should be same as data point size): "
              << data.getNoValues() << std::endl;
    std::cout << "data points: " << data.getNumDataPoints() << std::endl;

    const escript::DataReady_ptr &dataReadyPtr = data.borrowReadyPtr();
    const escript::DataTypes::RealVectorType &roVector = dataReadyPtr->getVectorRO();
    const escript::DataTypes::RealVectorType &typedRoVector = dataReadyPtr->getTypedVectorRO(0.);

    std::cout << "ro vector size: " << roVector.size() << std::endl;
    std::cout << "typed ro vector size: " << typedRoVector.size() << std::endl;
    std::cout << "inf: " << data.inf_const() << std::endl;
    std::cout << "sup: " << data.sup_const() << std::endl;
    std::cout << "Lsup: " << data.Lsup_const() << std::endl;
    std::cout << "sample data: " << std::endl;

    for (int i = 0; i  < std::min(2, data.getNumSamples()); ++i) {
        std::cout << "sample [" << i << "] :" << std::endl;

        for (int j = 0; j < data.getNumDataPointsPerSample(); ++j) {
            long offset = data.getDataOffset(i, j);

            if (rank == 0) {
                std::cout << "  roData [0]: "
                    << roVector[offset + 0] << std::endl;
            }
            else if (rank == 1) {
                for (int k = 0; k < dataShape[0]; ++k) {
                    std::cout << "  roData [" << k << "]: "
                        << roVector[offset + k] << std::endl;
                }
            }
            else if (rank == 2) {
                for (int k = 0; k < dataShape[0]; ++k) {
                    for (int l = 0; l < dataShape[1]; ++l) {
                        std::cout << "  roData [" << k << "," << l << "]: "
                            << roVector[offset +
                                escript::DataTypes::getRelIndex(dataShape, k, l)]
                            << std::endl;
                    }
                }
            }
            else if (rank == 3) {
                std::cout << "rank 3 is not supported" << std::endl;
            }
        }
    }
}

}  // namespace shirley

BOOST_PYTHON_MODULE(shirleycpp)
{
// This feature was added in boost v1.34
#if ((BOOST_VERSION/100)%1000 > 34) || (BOOST_VERSION/100000 >1)
    // params are: bool show_user_defined, bool show_py_signatures, bool show_cpp_signatures
    docstring_options docopt(true, true, false);
#endif

    // Register escript's default translators.
    // And yes, register_exception_translator() *DOES* need the & operator
    // in its sole parameter. Discussion at:
    //   https://wiki.python.org/moin/boost.python/RuntimeErrors
    REGISTER_ESCRIPT_EXCEPTION_TRANSLATORS;
    register_exception_translator<shirley::ShirleyException>(&escript::RuntimeErrorTranslator);

    scope().attr("__doc__") = "To use this module, please import shirley";
    scope().attr("BYTEORDER_NATIVE") = (int) shirley::BYTEORDER_NATIVE;
    scope().attr("BYTEORDER_LITTLE_ENDIAN") = (int) shirley::BYTEORDER_LITTLE_ENDIAN;
    scope().attr("BYTEORDER_BIG_ENDIAN") = (int) shirley::BYTEORDER_BIG_ENDIAN;
    scope().attr("DATATYPE_INT32") = (int) shirley::DATATYPE_INT32;
    scope().attr("DATATYPE_FLOAT32") = (int) shirley::DATATYPE_FLOAT32;
    scope().attr("DATATYPE_FLOAT64") = (int) shirley::DATATYPE_FLOAT64;

    // For an example of how to use the def() construct of Boost::Python, see:
    // https://www.boost.org/doc/libs/1_70_0/libs/python/doc/html/reference/high_level_components/boost_python_def_hpp.html#high_level_components.boost_python_def_hpp.example

    def("CheckShirleyVersion", shirley::_checkShirleyVersion,
      // No args!
      "Convenience method to check the version of the Shirley module.\n"
      ":rtype: ``int``");

    def("ReadGmsh", shirley::_readGmsh,
        (arg("filename") = "file.msh",
         arg("numDim") = 2,
         arg("order") = 3,
         arg("optimize") = true,
         arg("diracPoints") = list(),
         arg("diracTags") = list(),
         arg("traversalStrategy") = shirley::SPACE_FILLING_CURVE_TRAVERSAL),
        "Read a gmsh mesh file\n\n"
        ":rtype:`Domain`\n"
        ":param fileName: name of the mesh file\n:type fileName: ``string``\n"
        ":param numDim: number of dimensions of the mesh (has to be 2)\n:type numDim: ``int``\n"
        ":param order: polynomial order (>= 3, <= 10)\n:type order: ``int``\n"
        ":param optimize: Enable optimisation of node labels\n:type optimize: ``bool``\n"
        ":param diracPoints: Dirac points where wave sources are located\n:type diracPoints: ``list``\n"
        ":param diracTags: Tags for the Dirac points\n:type diracTags: ``list``\n",
        ":param traversalStrategy: Traversal strategy for mesh elements\n:type traversalStrategy: ``int``\n");

    def("PrintDataInternals", shirley::_printDataInternals,
        (arg("data")),
        "Print some internals of a Data instance\n");

    class_<shirley::ShirleyDomain, bases<escript::AbstractContinuousDomain>, boost::noncopyable >
        // no_init prevents ShirleyDomain from being directly instantiated
        // in Python. Use the factory methods instead!
        ("ShirleyDomain", "", no_init)
        .def("getX", &shirley::ShirleyDomain::getX,
            ":return: locations in the SEM nodes\n\n"
            ":rtype: `Data`")
        .def("clearInputSignal", &shirley::ShirleyDomain::clearInputSignal,
            "Clears the input signal of the PDE\n\n")
        .def("setRickerSignal", &shirley::ShirleyDomain::setRickerSignal,
            "Set the input signal of the PDE as a Ricker wavelet",
            (arg("f_dom") = 0.,
             arg("t_dom") = 0.,
             arg("signalDelay") = 0.),
            "Sets the input signal of the PDE as a Ricker wavelet\n\n"
            ":param f_dom: dominant frequency of the wavelet\n"
            ":param t_dom: time at which the wavelet is centered\n"
            ":param signalDelay: time delay to apply when computing the signal\n")
        .def("setPdeCoefficients", &shirley::ShirleyDomain::setPdeCoefficients,
            "Set the PDE coefficients that will be used in time integration",
            (arg("M") = escript::Data(),
             arg("B") = escript::Data(),
             arg("D") = escript::Data(),
             arg("E") = escript::Data(),
             arg("F") = escript::Data()),
            "Set the PDE coefficients that will be used in time integration\n\n"
            ":param M: M coefficient (multiplies du/dt when computing du/dt)\n"
            ":param B: B coefficient (multiplies v_l when computing du/dt)\n"
            ":param D: D coefficient (multiplies u when computing du/dt)\n"
            ":param E: E coefficient (multiplies v_l when computing dv_k/dt)\n"
            ":param F: F coefficient (multiplies u_{,l} when computing dv_k/dt)\n")
        .def("setInitialSolution", &shirley::ShirleyDomain::setInitialSolution,
            "Set the initial solution for the PDE",
            (arg("u") = escript::Data(),
             arg("v") = escript::Data(),
             arg("currentTime") = 0.),
            "Sets the initial solution for the PDE\n\n"
            ":param u: values for the continuous variable\n"
            ":param v: values for the discontinuous variable\n"
            ":param currentTime: current time of the simulation\n")
        .def("calculateTimeIntegrationMatrices",
            &shirley::ShirleyDomain::calculateTimeIntegrationMatrices,
            "Calculate the matrices that will speed up the time integration",
            (arg("timeStep") = 0.),
            "Calculates the matrices that will speed up the time integration\n\n"
            ":param timeStep: time step to use when building the matrices\n")
        //.def("fillJacobians",
        //    &shirley::ShirleyDomain::fillJacobians,
        //    "For each element, compute the Jacobian matrix, its determinant "
        //    "and its inverse\n\n")
        .def("progressToTime",
            &shirley::ShirleyDomain::progressToTime,
            "Progress the simulation until a certain point in time",
            (arg("finalTime") = 0.),
            "Makes the simulation progress until a certain point in time\n\n"
            ":param finalTime: point time when the simulation must end\n")
        .def("progressInSteps",
            &shirley::ShirleyDomain::progressInSteps,
            "Progress the simulation a certain number of steps in time",
            (arg("steps") = 0),
            "Makes the simulation progress a certain number of steps in time\n\n"
            ":param steps: how many steps in time the simulation should progress\n")
        .def("getU", &shirley::ShirleyDomain::getU,
            ":return: current value of u (continuous variable)\n\n"
            ":rtype: `Data`")
        .def("getV", &shirley::ShirleyDomain::getV,
            ":return: current value of v (discontinuous variable)\n\n"
            ":rtype: `Data`")
        .def("getCurrentTime", &shirley::ShirleyDomain::getCurrentTime,
            ":return: current simulation time\n\n"
            ":rtype: float");

    def("Ricker", shirley::_ricker,
        (arg("f_dom") = 0.,
         arg("t_dom") = 0.),
        "Create a Ricker wavelet\n\n"
        ":rtype:`Ricker`\n"
        ":param f_dom: Dominant frequency of the wavelet\n:type f_dom: ``double``\n"
        ":param t_dom: Time at which the wavelet is centered\n:type t_dom: ``double``\n");

    class_<shirley::Ricker>
        ("Ricker", "", init<double, double>())
        .def("getValue", &shirley::Ricker::getValue,
            "Gets the value of the Ricker wavelet for a specific moment in time",
            (arg("t") = 0.),
            "Gets the value of the Ricker wavelet for a specific moment in time\n\n"
            ":param t: time\n")
        .def("getVelocity", &shirley::Ricker::getVelocity,
            "Gets the velocity of the Ricker wavelet for a specific moment in time",
            (arg("t") = 0.),
            "Gets the velocity of the Ricker wavelet for a specific moment in time\n\n"
            ":param t: time\n")
        .def("getAcceleration", &shirley::Ricker::getAcceleration,
            "Gets the acceleration of the Ricker wavelet for a specific moment in time",
            (arg("t") = 0.),
            "Gets the acceleration of the Ricker wavelet for a specific moment in time\n\n"
            ":param t: time\n");
}
