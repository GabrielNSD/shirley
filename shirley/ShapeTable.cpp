
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/ShapeTable.h>

namespace shirley {

const char *getElementTypeName(ElementTypeId elemTypeId)
{
    switch (elemTypeId) {
        case Shirley_Point1:
            return "Point1";
        case Shirley_Line2:
            return "Line2";
        case Shirley_Tri3:
            return "Tri3";
        case Shirley_Tet4:
            return "Tet4";
        case Shirley_Line2Face:
            return "Line2Face";
        case Shirley_Tri3Face:
            return "Tri3Face";
        case Shirley_Tet4Face:
            return "Tet4Face";
        default:
            return "noElement";
    }
}

}  // namespace shirley
