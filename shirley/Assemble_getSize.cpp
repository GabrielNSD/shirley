
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/Assemble.h>
#include <shirley/Util.h>

#include <escript/index.h>

#include <sstream>
#include <cmath>

namespace shirley {

/// Calculates the minimum distance between two vertices of elements and
/// assigns the value to each item in `dest`
void Assemble_getSize(const NodeFile* nodes, const ElementFile* elements,
                      escript::Data& dest) {
    if ((nodes == nullptr) || (elements == nullptr)) {
        return;
    }

    if (dest.isComplex()) {
        throw escript::ValueError(
            "shirley::Assemble_getSize(): "
            "<dest> Data object can't be Complex");
    }

    // Check the dimensions of <dest>
    if (dest.getNumSamples() != elements->numElements) {
        throw escript::ValueError(
            "shirley::Assemble_getSize(): "
            "illegal number of samples in <dest> Data object");
    }

    if  (dest.getDataPointRank() != 0) {
        throw escript::ValueError(
            "shirley::Assemble_getSize(): "
            "<dest> Data object must have rank 0");
    }

    if (!dest.actsExpanded()) {
        throw escript::ValueError(
            "shirley::Assemble_getSize(): "
            "<dest> Data object must be expanded");
    }

    // Actual calculation
    const int numTotalNodesPerElem = elements->getNumTotalNodesPerElem();

    dest.requireWrite();

//#pragma omp for
    for (int el = 0; el < elements->numElements; ++el) {
        // Get the (x,y) coordinates of the vertex nodes for this element
        real_t x[3], y[3];

        index_t nodeId = elements->Nodes[elements->getVertexOffset(el, 0)];
        index_t arrayIdx = nodeId;
        x[0] = nodes->Coordinates[INDEX2(0, arrayIdx, nodes->numDim)];
        y[0] = nodes->Coordinates[INDEX2(1, arrayIdx, nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 1)];
        arrayIdx = nodeId;
        x[1] = nodes->Coordinates[INDEX2(0, arrayIdx, nodes->numDim)];
        y[1] = nodes->Coordinates[INDEX2(1, arrayIdx, nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 2)];
        arrayIdx = nodeId;
        x[2] = nodes->Coordinates[INDEX2(0, arrayIdx, nodes->numDim)];
        y[2] = nodes->Coordinates[INDEX2(1, arrayIdx, nodes->numDim)];

        // Compute distances between vertices
        real_t dist_1 = std::sqrt(std::pow(x[1] - x[0], 2.0)
            + std::pow(y[1] - y[0], 2.0));
        real_t dist_2 = std::sqrt(std::pow(x[2] - x[1], 2.0)
            + std::pow(y[2] - y[1], 2.0));
        real_t dist_3 = std::sqrt(std::pow(x[0] - x[2], 2.0)
            + std::pow(y[0] - y[2], 2.0));

        // Store minimum distance between vertices at all local nodes
        // for this element
        real_t min_dist = std::min({ dist_1, dist_2, dist_3 });

        for (int p = 0; p < numTotalNodesPerElem; ++p) {
            dest.getDataPointRW(el, p) = min_dist;
        }
    }  // for (int el = ...
}

} // namespace shirley
