
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

/****************************************************************************/

/*   Shirley: NodeFile : creates the mappings using the indexReducedNodes */
/*                  no distribution is happening                          */

/****************************************************************************/

#include <shirley/NodeFile.h>

namespace shirley {

void NodeFile::createDOFMappingAndCoupling()
{
    const index_t myFirstDOF = dofDistribution->getFirstComponent();
    const index_t myLastDOF = dofDistribution->getLastComponent();
    const int mpiSize = mpiInfo->size;
    const int myRank = mpiInfo->rank;

    index_t min_DOF, max_DOF;
    IndexPair DOF_range(util::getFlaggedMinMaxInt(
        numNodes, globalDegreesOfFreedom, -1));

    if (DOF_range.second < DOF_range.first) {
        min_DOF = myFirstDOF;
        max_DOF = myLastDOF - 1;
    } else {
        min_DOF = DOF_range.first;
        max_DOF = DOF_range.second;
    }

    int p_min = mpiSize;
    int p_max = -1;

    if (max_DOF >= min_DOF) {
        for (int p = 0; p < mpiSize; ++p) {
            if (dofDistribution->first_component[p] <= min_DOF) {
                p_min = p;
            }

            if (dofDistribution->first_component[p] <= max_DOF) {
                p_max = p;
            }
        }
    }

    std::stringstream ss;

    if ((myFirstDOF < myLastDOF) &&
            !((min_DOF <= myFirstDOF) && (myLastDOF - 1 <= max_DOF))) {
        ss << "NodeFile::createDOFMappingAndCoupling(): local elements do "
              "not span local degrees of freedom. "
              "min_DOF=" << min_DOF
           << ", myFirstDOF=" << myFirstDOF
           << ", myLastDOF-1=" << myLastDOF - 1
           << ", max_DOF=" << max_DOF
           << " on rank=" << mpiInfo->rank;
    }

    const std::string msg(ss.str());
    int error = static_cast<int>(msg.length());
    int gerror = error;
    escript::checkResult(error, gerror, mpiInfo);

    if (gerror > 0) {
        char* gmsg;
        escript::shipString(msg.c_str(), &gmsg, mpiInfo->comm);
        throw ShirleyException(gmsg);
    }

    const index_t UNUSED = -1;
    const dim_t len_loc_dof = max_DOF - min_DOF + 1;
    IndexVector shared(static_cast<unsigned long>(
        numNodes * (p_max - p_min + 1)));
    IndexVector locDOFMask((unsigned long) len_loc_dof);
    IndexVector nodeMask(static_cast<unsigned long>(numNodes));

#ifdef BOUNDS_CHECK
    ESYS_ASSERT((myLastDOF - min_DOF) <= len_loc_dof, "BOUNDS_CHECK");
#endif

#pragma omp parallel
    {
#pragma omp for
        for (index_t i = 0; i < len_loc_dof; ++i) {
            locDOFMask[i] = UNUSED;
        }

#pragma omp for
        for (index_t i = 0; i < numNodes; ++i) {
            nodeMask[i] = UNUSED;
        }

#pragma omp for
        for (index_t i = 0; i < numNodes; ++i) {
            const index_t k = globalDegreesOfFreedom[i];

            if (k > -1) {
#ifdef BOUNDS_CHECK
                ESYS_ASSERT((k - min_DOF) < len_loc_dof, "BOUNDS_CHECK");
#endif
                locDOFMask[k - min_DOF] = UNUSED - 1;
            }
        }

#pragma omp for
        for (index_t i = myFirstDOF - min_DOF; i < myLastDOF - min_DOF; ++i) {
            locDOFMask[i] = i - myFirstDOF + min_DOF;
        }
    }

    IndexVector wanted_DOFs(static_cast<unsigned long>(numNodes));
    IndexVector rcv_len((unsigned long) mpiSize);
    IndexVector snd_len((unsigned long) mpiSize);
    IntVector neighbour;
    IndexVector offsetInShared;
    dim_t n = 0;
    dim_t lastn = n;

    for (int p = p_min; p <= p_max; ++p) {
        if (p != myRank) {
            const index_t firstDOF =
                std::max(min_DOF, dofDistribution->first_component[p]);
            const index_t lastDOF =
                std::min(max_DOF + 1, dofDistribution->first_component[p + 1]);

#ifdef BOUNDS_CHECK
            ESYS_ASSERT((lastDOF - min_DOF) <= len_loc_dof, "BOUNDS_CHECK");
#endif

            for (index_t i = firstDOF - min_DOF; i < lastDOF - min_DOF; ++i) {
                if (locDOFMask[i] == UNUSED - 1) {
                    locDOFMask[i] = myLastDOF - myFirstDOF + n;
                    wanted_DOFs[n] = i + min_DOF;
                    ++n;
                }
            }

            if (n > lastn) {
                rcv_len[p] = n - lastn;
                neighbour.push_back(p);
                offsetInShared.push_back(lastn);
                lastn = n;
            }
        }  // if p!=myRank
    }  // for p

    offsetInShared.push_back(lastn);

    // Assign new DOF labels to nodes
//#pragma omp parallel for
    for (index_t i = 0; i < numNodes; ++i) {
        const index_t k = globalDegreesOfFreedom[i];

        if (k > -1) {
            nodeMask[i] = locDOFMask[k - min_DOF];
        }
    }

    degreesOfFreedomMapping.assign(&nodeMask[0], numNodes, UNUSED);

    // Define how to get DOF values for controlled but other processors
#ifdef BOUNDS_CHECK
    ESYS_ASSERT((numNodes == 0) ||
        (offsetInShared.back() < (numNodes * (p_max - p_min + 1))), "BOUNDS_CHECK");
#endif

#pragma omp parallel for
    for (index_t i = 0; i < lastn; ++i) {
        shared[i] = myLastDOF - myFirstDOF + i;
    }

#ifdef ESYS_HAVE_PASO
    paso::SharedComponents_ptr rcv_shcomp(new paso::SharedComponents(
        myLastDOF - myFirstDOF, neighbour, &shared[0], offsetInShared));
#endif

    /////////////////////////////////
    //   Now we build the sender   //
    /////////////////////////////////
#ifdef ESYS_MPI
    std::vector<MPI_Request> mpi_requests(mpiSize * 2);
    std::vector<MPI_Status> mpi_stati(mpiSize * 2);

    MPI_Alltoall(&rcv_len[0], 1, MPI_DIM_T, &snd_len[0], 1, MPI_DIM_T,
                 mpiInfo->comm);

    int count = 0;

    for (int p = 0; p < neighbour.size(); p++) {
        MPI_Isend(&wanted_DOFs[offsetInShared[p]],
                  offsetInShared[p + 1] - offsetInShared[p],
                  MPI_DIM_T, neighbour[p], mpiInfo->counter() + myRank,
                  mpiInfo->comm, &mpi_requests[count]);
        count++;
    }
#else
    snd_len[0] = rcv_len[0];
#endif

    n = 0;
    neighbour.clear();
    offsetInShared.clear();

#ifdef ESYS_MPI
    for (int p = 0; p < mpiSize; p++) {
        if (snd_len[p] > 0) {
            MPI_Irecv(&shared[n], snd_len[p], MPI_DIM_T, p,
                      mpiInfo->counter() + p, mpiInfo->comm,
                      &mpi_requests[count]);
            count++;

            neighbour.push_back(p);
            offsetInShared.push_back(n);

            n += snd_len[p];
        }
    }

    mpiInfo->incCounter(mpiInfo->size);
    MPI_Waitall(count, &mpi_requests[0], &mpi_stati[0]);
#endif

    offsetInShared.push_back(n);

    // Map global IDs to local IDs
#pragma omp parallel for
    for (index_t i = 0; i < n; ++i) {
        shared[i] = locDOFMask[shared[i] - min_DOF];
    }

#ifdef ESYS_HAVE_PASO
    paso::SharedComponents_ptr snd_shcomp(new paso::SharedComponents(
        myLastDOF - myFirstDOF, neighbour, &shared[0], offsetInShared));
    degreesOfFreedomConnector.reset(new paso::Connector(snd_shcomp, rcv_shcomp));
#endif
}

void NodeFile::createNodeMappings(const IndexVector& dofDist,
                                  const IndexVector& nodeDist)
{
    // ==== distribution of Nodes ====
    nodesDistribution.reset(new escript::Distribution(mpiInfo, nodeDist));

    // ==== distribution of DOFs ====
    dofDistribution.reset(new escript::Distribution(mpiInfo, dofDist));

    IndexVector nodeMask(static_cast<unsigned long>(numNodes));
    const index_t UNUSED = -1;

    // ==== nodes mapping (dummy) ====
#pragma omp parallel for
    for (index_t n = 0; n < numNodes; ++n) {
        nodeMask[n] = n;
    }

    nodesMapping.assign(&nodeMask[0], numNodes, UNUSED);

    // ==== mapping between nodes and DOFs + DOF connector ====
    createDOFMappingAndCoupling();

    // get the IDs for DOFs
#pragma omp parallel for
    for (index_t i = 0; i < degreesOfFreedomMapping.numTargets; ++i) {
        degreesOfFreedomId[i] = Id[degreesOfFreedomMapping.map[i]];
    }
}

}  // namespace shirley
