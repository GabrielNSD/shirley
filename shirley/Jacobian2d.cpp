
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/Jacobian2d.h>

namespace shirley {

/// Computes the elements of the Jacobian matrix, its determinant and
/// the elements of the inverse matrix
Jacobian2d::Jacobian2d(
    double x1, double y1, double x2, double y2, double x3, double y3)
{
    m_a = x2 - x1;  // ∂x/∂ξ = x2 - x1
    m_b = y2 - y1;  // ∂y/∂ξ = y2 - y1
    m_c = x3 - x1;  // ∂x/∂η = x3 - x1
    m_d = y3 - y1;  // ∂y/∂η = y3 - y1

    m_determinant = (m_a * m_d) - (m_b * m_c);

    if (m_determinant == 0.0) {
        throw ShirleyException(
            "Jacobian2d::ctor: Jacobian matrix determinant cannot be zero");
    }

    m_inv_a = (1.0 / m_determinant) *  m_d;
    m_inv_b = (1.0 / m_determinant) * -m_b;
    m_inv_c = (1.0 / m_determinant) * -m_c;
    m_inv_d = (1.0 / m_determinant) *  m_a;
}

}  // namespace shirley
