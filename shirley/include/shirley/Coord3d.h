
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_COORD3D_H__
#define __SHIRLEY_COORD3D_H__


namespace shirley {

/**
 * Simple class to hold the spatial coordinates of a 3D point.
 */

class Coord3d {
public:
    Coord3d() :
        m_x(0.0), m_y(0.0), m_z(0.0) { }

    Coord3d(double x, double y, double z) :
        m_x(x), m_y(y), m_z(z) { }

    ~Coord3d() = default;

public:
    double m_x;

    double m_y;

    double m_z;
};

}  // namespace shirley


#endif  // __SHIRLEY_COORD3D_H__
