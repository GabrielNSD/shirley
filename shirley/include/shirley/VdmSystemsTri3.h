
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_VDM_SYSTEMS_TRI3_H__
#define __SHIRLEY_VDM_SYSTEMS_TRI3_H__

namespace shirley {

/**
 * Non-instantiable, utility class with static methods related to
 * Vandermonde systems for triangular domains. These Vandermonde systems
 * yield coefficients that can be used to interpolate functions over
 * triangles with high accuracy. Detailed discussions can be found at
 * doi:10.1093/imamat/hxh077.
 */

class VdmSystemsTri3 {
private:
    // Default constructor is hidden to prevent instantiation
    VdmSystemsTri3() = default;

public:
    // This function can be used to compute the coefficients of cardinal
    // node interpolation functions (Ψ_i / psi_i) of a given polynomial degree
    // and psi-index (i.e. the "i" subscript in psi_i) by solving
    // Vandermonde systems based on Proriol or Appell polynomials.
    //
    // It is here for documentation purposes only, as it has already been
    // used to compute the coefficients for polynomial degrees 3 through 10
    // and the resulting values are tabulated in VdmSystemsTri3.cpp.
    static void computeVdmCoefficients(
        int polyDegree, int psiIndex, std::stringstream &dest,
        bool roundResultsTowardZero, double roundingTolerance = 1E-14);

    // <dest> must be able to hold at least
    // (1/2)*(<polyDegree+1>)*(<polyDegree+2>) elements
    static void fillVdmCoefficients(int polyDegree, int psiIndex, double *dest);

    static double getIntegrationWeight(int polyDegree, int psiIndex);

    // <dest> must be able to hold at least
    // (1/2)*(<polyDegree+1>)*(<polyDegree+2>) elements
    static void fillIntegrationWeights(int polyDegree, double *dest);
};

}


#endif // __SHIRLEY_VDM_SYSTEMS_TRI3_H__
