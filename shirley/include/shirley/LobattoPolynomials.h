
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_LOBATTOPOLYNOMIALS_H__
#define __SHIRLEY_LOBATTOPOLYNOMIALS_H__


#include <vector>
#include <utility>
#include <array>

#include <cmath>

#include "ShirleyException.h"

namespace shirley {

/**
 * Non-instantiable, utility class with static methods related to Lobatto
 * polynomials. In our use case, as we employ the roots of Lobatto polynomials
 * to compute the ν (nu), ξ (xi) and η (eta) values used in conjunction
 * with Appell polynomials as described in doi:10.1093/imamat/hxh077, this
 * class has several member functions to deal with Appell polynomials as
 * well. There are also some functions that convert between the (x,y)
 * coordinates of "general" triangles, which can take any value, and the
 * (ξ,η) coordinates of "standard" triangles, with ξ and η >= 0 and
 * (ξ + η) <= 1.0. This is done through nodal shape functions, which are
 * described in the lecture "Quadrature Formulas in Two Dimensions" by
 * Shaozhong Deng of UNC at Charlotte.
 */

class LobattoPolynomials {
private:
    // Default constructor is hidden to prevent instantiation
    LobattoPolynomials() = default;

public:
    // <dest> must be able to hold at least <polyDegree> elements
    static void fillLobattoPolynomialRoots(int polyDegree, double *dest);

    // <dest> must be able to hold at least <polyDegree+1> elements
    static void fillNuArrayTri3(int polyDegree, double *dest);

    // <xi> and <eta> must be able to hold at least
    // (1/2)*(<polyDegree+1>)*(<polyDegree+2>) elements each
    static void fillXiAndEtaArraysTri3(int polyDegree, double *xi, double *eta);

    static double calcAppellCoefficient(int k, int l, int i, int j);

    static double calcAppellPolynomial(
        int k, int l, double xi, double eta, bool roundResultTowardZero,
        double roundingTolerance = 1E-14);

    static void sortXiAndEtaPairs(
        int arraySize, const double *xi, const double *eta,
        std::vector<std::pair<double, double>>& dest);

    static double calcDerivativeOfAppellPolynomialWithRespectToXi(
        int k, int l, double xi, double eta, bool roundResultTowardZero,
        double roundingTolerance = 1E-14);

    static double calcDerivativeOfAppellPolynomialWithRespectToEta(
        int k, int l, double xi, double eta, bool roundResultTowardZero,
        double roundingTolerance = 1E-14);

    // <dest> must be able to hold at least two elements
    static void calcDerivativeOfAppellPolynomialWithRespectToXiAndEta(
        int k, int l, double xi, double eta, double *dest,
        bool roundResultTowardZero, double roundingTolerance = 1E-14);

    static double nodalShapeFunctionN1(double xi, double eta);

    static double nodalShapeFunctionN2(double xi, double eta);

    static double nodalShapeFunctionN3(double xi, double eta);

    static double calcXFromStandardToGeneralTriangle(
        double xi, double eta, double x1, double x2, double x3);

    static double calcYFromStandardToGeneralTriangle(
        double xi, double eta, double y1, double y2, double y3);

    static double calcTriangleArea(
        double x1, double x2, double x3, double y1, double y2, double y3);

    static double calcXiFromGeneralToStandardTriangle(
        double x, double y, double x1, double x3, double y1, double y3,
        double triangleArea, bool roundResultTowardZero,
        double roundingTolerance = 1E-14);

    static double calcEtaFromGeneralToStandardTriangle(
        double x, double y, double x1, double x2, double y1, double y2,
        double triangleArea, bool roundResultTowardZero,
        double roundingTolerance = 1E-14);
};

// See "Quadrature Formulas in Two Dimensions", by Shaozhong Deng
// (UNC at Charlotte), section C.2 (pg. 11)
inline
double LobattoPolynomials::nodalShapeFunctionN1(double xi, double eta)
{
    return 1.0 - xi - eta;
}

// See "Quadrature Formulas in Two Dimensions", by Shaozhong Deng
// (UNC at Charlotte), section C.2 (pg. 11)
inline
double LobattoPolynomials::nodalShapeFunctionN2(double xi, double eta)
{
    return xi;
}

// See "Quadrature Formulas in Two Dimensions", by Shaozhong Deng
// (UNC at Charlotte), section C.2 (pg. 11)
inline
double LobattoPolynomials::nodalShapeFunctionN3(double xi, double eta)
{
    return eta;
}

// See "Quadrature Formulas in Two Dimensions", by Shaozhong Deng
// (UNC at Charlotte), section C.2 (pg. 11)
inline
double LobattoPolynomials::calcXFromStandardToGeneralTriangle(
    double xi, double eta, double x1, double x2, double x3)
{
    return (x1 * nodalShapeFunctionN1(xi, eta))
        + (x2 * nodalShapeFunctionN2(xi, eta))
        + (x3 * nodalShapeFunctionN3(xi, eta));
}

// See "Quadrature Formulas in Two Dimensions", by Shaozhong Deng
// (UNC at Charlotte), section C.2 (pg. 11)
inline
double LobattoPolynomials::calcYFromStandardToGeneralTriangle(
    double xi, double eta, double y1, double y2, double y3)
{
    return (y1 * nodalShapeFunctionN1(xi, eta))
        + (y2 * nodalShapeFunctionN2(xi, eta))
        + (y3 * nodalShapeFunctionN3(xi, eta));
}

// See "Quadrature Formulas in Two Dimensions", by Shaozhong Deng
// (UNC at Charlotte), section C.2 (pg. 11)
inline
double LobattoPolynomials::calcTriangleArea(
    double x1, double x2, double x3, double y1, double y2, double y3)
{
    return fabs((x1 * (y2 - y3)) + (x2 * (y3 - y1)) + (x3 * (y1 - y2))) * 0.5;
}

// See "Quadrature Formulas in Two Dimensions", by Shaozhong Deng
// (UNC at Charlotte), section C.2 (pg. 12)
inline
double LobattoPolynomials::calcXiFromGeneralToStandardTriangle(
        double x, double y, double x1, double x3, double y1, double y3,
        double triangleArea, bool roundResultTowardZero,
        double roundingTolerance)
{
    double result =
        (((y3 - y1) * (x - x1)) - ((x3 - x1) * (y - y1)))
        / (2.0 * triangleArea);

    if (roundResultTowardZero && (fabs(result) <= roundingTolerance)) {
        result = 0.0;
    }

    return result;
}

// See "Quadrature Formulas in Two Dimensions", by Shaozhong Deng
// (UNC at Charlotte), section C.2 (pg. 12)
inline
double LobattoPolynomials::calcEtaFromGeneralToStandardTriangle(
        double x, double y, double x1, double x2, double y1, double y2,
        double triangleArea, bool roundResultTowardZero,
        double roundingTolerance)
{
    double result =
        ((-1.0 * (y2 - y1) * (x - x1)) + ((x2 - x1) * (y - y1)))
        / (2.0 * triangleArea);

    if (roundResultTowardZero && (fabs(result) <= roundingTolerance)) {
        result = 0.0;
    }

    return result;
}

}  // namespace shirley


#endif // __SHIRLEY_LOBATTOPOLYNOMIALS_H__
