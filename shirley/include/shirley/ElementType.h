
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_ELEMENTTYPE_H__
#define __SHIRLEY_ELEMENTTYPE_H__


#include <string>

namespace shirley {

typedef enum {
    Shirley_Point1 = 0,
    Shirley_Line2 = 1,
    Shirley_Tri3 = 2,
    Shirley_Tet4 = 3,
    Shirley_Line2Face = 4,
    Shirley_Tri3Face = 5,
    Shirley_Tet4Face = 6,
    Shirley_NoRef = 7  // Marks end of list
} ElementTypeId;

inline
ElementTypeId elementTypeIdFromString(const std::string &s)
{
    if (s == "Point1")
        return Shirley_Point1;
    if (s == "Line2")
        return Shirley_Line2;
    if (s == "Tri3")
        return Shirley_Tri3;
    if (s == "Tet4")
        return Shirley_Tet4;
    if (s == "Line2Face")
        return Shirley_Line2Face;
    if (s == "Tri3Face")
        return Shirley_Tri3Face;
    if (s == "Tet4Face")
        return Shirley_Tet4Face;

    return Shirley_NoRef;
}

}  // namespace shirley


#endif  // __SHIRLEY_ELEMENTTYPE_H__
