
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_ELEMENTFILE_H__
#define __SHIRLEY_ELEMENTFILE_H__


#include <shirley/Shirley.h>
#include <shirley/NodeFile.h>
#include <shirley/ElementType.h>
#include <shirley/Util.h>
#include <shirley/Coord3d.h>

#include <escript/index.h>

namespace shirley {

class ElementFile
{
public:
    ElementFile(ElementTypeId elementTypeId, escript::JMPI mpiInfo,
                int order);

    ~ElementFile();

    /// Allocates the element table within an element file to hold
    /// <new_numElements> elements
    void allocTable(dim_t new_numElements);

    /// Deallocates the element table within an element file
    void freeTable();

    /// Copies element file `in` into this element file starting from `offset`.
    /// The elements `offset` to in->numElements+offset-1 will be overwritten.
    void copyTable(index_t offset, index_t nodeOffset, index_t idOffset,
                   const ElementFile* in);

    /// Prints information about this element file to stdout
    void print(const index_t* nodesId) const;

    /// Redistributes the elements including overlap by rank
    void distributeByRankOfDOF(const int* mpiRankOfDOF,
                               const index_t* nodesId);

    /// Tries to reduce the number of colors used to color elements in this
    /// ElementFile
    void createColoring(dim_t dofMap_numNodes, const index_t* dofMap);

    /// Reorders the elements so that they are stored close to the nodes
    void optimizeOrdering();

    /// Assigns new node reference numbers to the elements.
    /// If k is the old node, the new node is newNodes[k-offset].
    void relabelNodes(const IndexVector& newNodes, index_t offset);

    void markNodes(IntVector& mask, index_t offset) const;

    /// Gathers the elements from the element file `in` using
    /// index[0:out->elements-1]. `index` has to be between 0 and
    /// in->numElements-1. A conservative assumption on the colouring is made.
    void gather(const index_t* index, const ElementFile* in);

    /// Sets element tags to newTag where mask>0
    void setTags(int newTag, const escript::Data& mask);

    /// Returns the minimum and maximum reference number of nodes
    /// describing the elements
    inline IndexPair getNodeRange() const;

    inline IndexPair getElementIdRange() const;

    inline void updateTagList();

    /// Calculates the total number of nodes per element
    /// (vertices + inner nodes at the edges + inner nodes at the faces +
    ///  interior nodes)
    int calcNumTotalNodesPerElem() const;

    /// Reads the total number of nodes per element
    inline int getNumTotalNodesPerElem() const { return m_numTotalNodesPerElem; }

    /// Checks whether elements in this ElementFile have SEM nodes
    /// (i.e. nodes other than vertices)
    bool hasSemNodes() const;

    /// Given an element number, calculates the memory offset of one of
    /// its vertices in the Nodes array.
    /// <elementNum> and <vertexNum> are zero-based.
    int getVertexOffset(int elementNum, int vertexNum) const;

    /// Given an element number, calculates the memory offset of an inner
    /// node of one of its edges in the Nodes array.
    /// Should *NOT* be used for vertices.
    /// <elementNum>, <edgeNum> and <innerEdgeNodeNum> are zero-based.
    int getInnerEdgeNodeOffset(int elementNum, int edgeNum,
                               int innerEdgeNodeNum) const;

    /// Given an element number, calculates the memory offset of an inner
    /// node of one of its faces in the Nodes array.
    /// Should *NOT* be used for vertices.
    /// <elementNum>, <faceNum> and <innerFaceNodeNum> are zero-based.
    int getInnerFaceNodeOffset(int elementNum, int faceNum,
                               int innerFaceNodeNum) const;

    /// Given an element number, calculates the memory offset of one of
    /// its interior nodes in the Nodes array.
    /// <elementNum> and <interiorNodeNum> are zero-based.
    int getInteriorNodeOffset(int elementNum, int interiorNodeNum) const;

private:
    void swapTable(ElementFile* other);

public:
    escript::JMPI mpiInfo;

    /// Number of elements
    dim_t numElements;

    /// Id[i] is the id number of element i. This number is used when elements
    /// are resorted. In the entire code the term 'element id' refers to i and
    /// not to Id[i] unless explicitly stated otherwise.
    index_t* Id;

    /// Tag[i] is the tag of element i
    int* Tag;

    /// Owner[i] contains the rank that owns element i
    int* Owner;

    /// Array of tags which are actually used
    IntVector tagsInUse;

    /// Number of vertices per element
    int m_numVerticesPerElem;

    /// Nodes[INDEX2(k,i,m_numTotalNodesPerElem)] is the k-th node in
    /// the i-th element.
    ///
    /// Assuming <n> elements,
    /// <v> vertices per element,
    /// <e> edges (possibly zero) per element,
    /// <ie> inner nodes (possibly zero) per edge per element,
    /// <f> faces (possibly zero) per element,
    /// <if> inner nodes (possibly zero) per face per element, and
    /// <in> interior nodes (possibly zero) per element,
    /// memory layout is as follows:
    ///
    /// For each element <N> in <n>:
    ///   For each vertex <V> in <v>:
    ///     [Vertex <V> of element <N>]
    ///   For each edge <E> in <e>:
    ///     For each inner node <IE> in <ie>:
    ///       [Inner node <IE> of edge <E> of element <N>]
    ///   For each face <F> in <f>:
    ///     For each inner node <IF> in <if>:
    ///       [Inner node <IF> of face <F> of element <N>]
    ///   For each interior node <IN> in <in>:
    ///     [Interior node <IN> of element <N>]
    index_t* Nodes;

    /// Assigns each element a color. Elements with the same color don't share
    /// a node so they can be processed simultaneously. At any time Color must
    /// provide a valid value. In any case one can set Color[e]=e for all e.
    index_t* Color;

    /// Minimum color value
    index_t minColor;

    /// Maximum color value
    index_t maxColor;

    /// Number of spatial dimensions of the domain
    int numDim;

    /// Dimension of the element e.g. 2 for a line in 2D or 3D
    int numLocalDim;

    /// Element type ID
    ElementTypeId elemTypeId;

    /// Name of element type
    const char *elemTypeName;

    /// Number of shape functions
    int numShapes;

    /// Element order with respect to SEM (i.e. the degree of the Legendre
    /// polynomial used).
    /// Each element will have <m_order + 1> points in each axis.
    int m_order;

    /// Number of edges per element
    int m_numEdgesPerElem;

    /// Number of inner SEM nodes per edge.
    /// Does *NOT* include the vertices of the edge.
    int m_numInnerNodesPerEdge;

    /// Number of faces per element
    int m_numFacesPerElem;

    /// Number of inner SEM nodes per face.
    /// Does *NOT* include the vertices of the face.
    int m_numInnerNodesPerFace;

    /// Number of interior nodes per element (only makes sense in 3D)
    int m_numInteriorNodesPerElem;

    /// Total number of nodes per element
    /// (vertices + inner nodes at the edges + inner nodes at the faces +
    /// interior nodes).
    /// Should be assigned with a call to calcNumTotalNodesPerElem().
    int m_numTotalNodesPerElem;
};

inline IndexPair ElementFile::getNodeRange() const
{
    return util::getMinMaxInt(getNumTotalNodesPerElem(), numElements, Nodes);
}

inline IndexPair ElementFile::getElementIdRange() const
{
    return util::getMinMaxInt(1, numElements, Id);
}

inline void ElementFile::updateTagList()
{
    util::setValuesInUse(Tag, numElements, tagsInUse, mpiInfo);
}

}  // namespace shirley


#endif  // __SHIRLEY_ELEMENTFILE_H__
