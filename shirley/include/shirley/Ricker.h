
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_RICKER_H__
#define __SHIRLEY_RICKER_H__


#include "IWavelet.h"

#include <escript/Pointers.h>

namespace shirley {

// The Ricker Wavelet w=f(t)

class Ricker;

typedef POINTER_WRAPPER_CLASS(Ricker) Ricker_ptr;

class Ricker : public IWavelet {
public:
  double m_drop;

  double m_f;

  double m_f_max;

  double m_s;

  double m_t0;

  explicit Ricker(double f_dom, double t_dom = 0.0);

  virtual ~Ricker() = default;

  double getCenter();

  double getTimeScale();

  virtual double getValue(double t) override;

  virtual double getVelocity(double t) override;

  virtual double getAcceleration(double t) override;
};

}  // namespace shirley


#endif //__SHIRLEY_RICKER_H__
