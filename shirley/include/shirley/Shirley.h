
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_SHIRLEY_H__
#define __SHIRLEY_SHIRLEY_H__


/****************************************************************************/

/*    Shirley spectral element solver */

/****************************************************************************/

#include <shirley/ShirleyException.h>

#include <escript/DataTypes.h>
#include <escript/Data.h>
#include <escript/EsysMPI.h>

namespace shirley {

using escript::DataTypes::index_t;
using escript::DataTypes::dim_t;
using escript::DataTypes::IndexVector;

typedef std::vector<int> IntVector;
typedef std::vector<short> ShortVector;
typedef std::vector<double> DoubleVector;
typedef std::vector<bool> BoolVector;

using escript::DataTypes::real_t;
using escript::DataTypes::cplx_t;

typedef std::vector<escript::DataTypes::real_t> RealVector;
typedef std::vector<escript::DataTypes::cplx_t> ComplexVector;

typedef std::pair<index_t, index_t> IndexPair;

typedef unsigned char u_char;

#define SHIRLEY_UNKNOWN -1
#define SHIRLEY_DEGREES_OF_FREEDOM 1
#define SHIRLEY_NODES 3
#define SHIRLEY_ELEMENTS 4
#define SHIRLEY_FACE_ELEMENTS 5
#define SHIRLEY_POINTS 6
// Currently unused
#define SHIRLEY_REDUCED_DEGREES_OF_FREEDOM 2
// Currently unused
#define SHIRLEY_REDUCED_NODES 14
// Currently unused
#define SHIRLEY_REDUCED_ELEMENTS 10
// Currently unused
#define SHIRLEY_REDUCED_FACE_ELEMENTS 11

// Codes for function space types supported
enum {
    DegreesOfFreedom = SHIRLEY_DEGREES_OF_FREEDOM,
    // Currently unused
    ReducedDegreesOfFreedom = SHIRLEY_REDUCED_DEGREES_OF_FREEDOM,
    Nodes = SHIRLEY_NODES,
    // Currently unused
    ReducedNodes = SHIRLEY_REDUCED_NODES,
    Elements = SHIRLEY_ELEMENTS,
    // Currently unused
    ReducedElements = SHIRLEY_REDUCED_ELEMENTS,
    FaceElements = SHIRLEY_FACE_ELEMENTS,
    // Currently unused
    ReducedFaceElements = SHIRLEY_REDUCED_FACE_ELEMENTS,
    Points = SHIRLEY_POINTS
};

inline bool hasReducedIntegrationOrder(const escript::Data& in)
{
    const int fs = in.getFunctionSpace().getTypeCode();
    return (fs == ReducedElements) || (fs == ReducedFaceElements);
}

#define SHIRLEY_INITIAL_STATUS 0

}  // namespace shirley


#endif  // __SHIRLEY_SHIRLEY_H__
