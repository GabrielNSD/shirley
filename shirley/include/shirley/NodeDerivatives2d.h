
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_NODEDERIVATIVES2D_H__
#define __SHIRLEY_NODEDERIVATIVES2D_H__


#include <vector>

#include "ShirleyException.h"

namespace shirley {

/**
 * Class to store the partial derivatives of basis functions Ψ_i in
 * relation to spatial directions ξ (xi) and η (eta) for a SEM node
 * located at a quadrature point (ξ,η) within an element.
 * The expressions for Ψ_i(ξ,η) are those of Eqs. (1.8) and (3.2) at
 * doi:10.1093/imamat/hxh077 (looking at Eq. (1.4) is also a good idea).
 */

class NodeDerivatives2d {
public:
    // A no-parameter constructor is provided so that this class can
    // be used as a value in instances of std::map, std::unordered_map
    // and the like
    NodeDerivatives2d() = default;

    explicit NodeDerivatives2d(int numNodes) :
        m_dpsi_dxi(numNodes),
        m_dpsi_deta(numNodes) { }

    ~NodeDerivatives2d() {
        m_dpsi_dxi.clear();
        m_dpsi_deta.clear();
    }

public:
    // <m_dpsi_dxi> and <m_dpsi_deta> store the partial derivatives of
    // Ψ_i(ξ,η), for a specific (ξ,η) quadrature point within an element.
    // Each item in <m_dpsi_dxi> and <m_dpsi_deta> corresponds to a
    // Ψ_i basis function.
    // Both <m_dpsi_dxi> and <m_dpsi_deta> have as many items for a
    // (ξ,η) quadrature point as the total number of quadrature points
    // contained in an element. This is dictated by SEM, where quadrature
    // points double as numerical integration points.

    std::vector<double> m_dpsi_dxi;  // Terms of ∂Ψ_i/∂ξ

    std::vector<double> m_dpsi_deta;  // Terms of ∂Ψ_i/∂η
};

}  // namespace shirley


#endif //__SHIRLEY_NODEDERIVATIVES2D_H__
