
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_NODEFILE_H__
#define __SHIRLEY_NODEFILE_H__


#include <shirley/Shirley.h>
#include <shirley/NodeMapping.h>

#include <escript/Distribution.h>

namespace shirley {

// Node types
const u_char NodeType_Vertex = 1;
const u_char NodeType_SemNodeOverAnEdge = 2;
const u_char NodeType_SemNodeOverAFace = 3;
const u_char NodeType_SemNodeInsideAnElement = 4;
const u_char NodeType_Unknown = 5;

class NodeFile {
public:
    /// Constructor - creates empty node file.
    /// Use allocTable() to allocate the node table (Id, Coordinates, etc).
    NodeFile(int nDim, escript::JMPI mpiInfo);

    /// Destructor
    ~NodeFile();

    /// Allocates the node table within this node file to hold <new_numNodes>
    /// nodes.
    void allocTable(dim_t new_numNodes);

    /// Empties the node table and frees all memory
    void freeTable();

    void print() const;

    index_t getFirstNode() const;

    index_t getLastNode() const;

    dim_t getGlobalNumNodes() const;

    /// Returns the number of SEM nodes (on this rank)
    inline dim_t getNumNodes() const;

    /// returns the number of degrees of freedom (on this rank)
    inline dim_t getNumDegreesOfFreedom() const;

    /// returns the number of degrees of freedom targets (own and shared)
    inline dim_t getNumDegreesOfFreedomTargets() const;

    /// returns the mapping from target to the local degrees of freedom
    inline const index_t* borrowDegreesOfFreedomTarget() const;

    /// returns the mapping from local degrees of freedom to a target
    inline const index_t* borrowTargetDegreesOfFreedom() const;

    /// Gathers nodes from the NodeFile `in` using the entries in
    /// index[0:numNodes-1] which are between min_index and max_index
    /// (exclusive)
    void gather(const index_t* index, const NodeFile* in);

    void gather_global(const index_t* index, const NodeFile* in);

    inline void updateTagList();

    /// Creates a dense labeling of the global degrees of freedom and returns
    /// the new number of global degrees of freedom
    dim_t createDenseDOFLabeling();

    dim_t createDenseNodeLabeling(IndexVector& nodeDistribution,
                                  const IndexVector& dofDistribution);

    void createNodeMappings(const IndexVector& dofDistribution,
                            const IndexVector& nodeDistribution);

    void assignMPIRankToDOFs(int* mpiRankOfDOF,
                             const IndexVector& distribution);

    /// set tags to newTag where mask>0
    void setTags(int newTag, const escript::Data& mask);

    IndexPair getDOFRange() const;

    /// Expands the capacity of the node file
    void addNodeCapacity(dim_t numNodesToAdd);

    /// Number of nodes
    dim_t numNodes;

    NodeMapping nodesMapping;
    NodeMapping degreesOfFreedomMapping;

public:
    /// MPI information
    escript::JMPI mpiInfo;

    /// Number of spatial dimensions
    int numDim;

    /// Id[i] is the unique ID number of SEM node i
    index_t* Id;

    /// Tag[i] is the tag of node i
    int* Tag;

    /// globalDegreesOfFreedom[i] is the global degree of freedom assigned
    /// to node i. This index is used to consider periodic boundary conditions
    /// by assigning the same degree of freedom to different nodes.
    index_t* globalDegreesOfFreedom;

    /// Coordinates[INDEX2(k,i,numDim)] is the k-th coordinate of node i
    /// over a "general" triangle (in 2-D) or a "general" tetrahedron
    /// (in 3-D), i.e. x, y and z coordinates that may take any value.
    real_t* Coordinates;

    /// XiEtaCoordinates[INDEX2(k,i,numDim)] is the k-th coordinate of
    /// node i over a "standard" triangle (in 2-D) or a "standard"
    /// tetrahedron (in 3-D), i.e. ξ and η coordinates with ξ>=0, η>=0
    /// and ξ+η<=1 (in 2-D), or ξ, η and ζ coordinates with ξ>=0, η>=0,
    /// ζ>=0 and ξ+η+ζ<=1 (in 3-D).
    real_t* XiEtaCoordinates;

    /// Assigns each local node a global unique ID in a dense labeling
    index_t* globalNodesIndex;

    /// These are the packed versions of Id
    index_t* degreesOfFreedomId;

    // m_nodeType[i] is the type of node i (see the NodeType enum).
    //   1 = Vertex node
    //   2 = SEM node over an edge
    //   3 = SEM node over a face
    //   4 = SEM node inside an element
    //   5 = Unknown node type
    u_char* m_nodeType;

    /// MPI distribution of nodes
    escript::Distribution_ptr nodesDistribution;

    /// MPI distribution of degrees of freedom
    escript::Distribution_ptr dofDistribution;

    /// The status counts the updates done on the node coordinates.
    /// The value is increased by 1 when the node coordinates are updated.
    int status;

    /// Vector of tags which are actually used
    IntVector tagsInUse;

private:
    IndexPair getGlobalIdRange() const;

    IndexPair getGlobalDOFRange() const;

    void createDOFMappingAndCoupling();
};

//
// Implementation of inline methods
//

inline index_t NodeFile::getFirstNode() const
{
    return nodesDistribution->getFirstComponent();
}

inline index_t NodeFile::getLastNode() const
{
    return nodesDistribution->getLastComponent();
}

inline dim_t NodeFile::getGlobalNumNodes() const
{
    return nodesDistribution->getGlobalNumComponents();
}

inline dim_t NodeFile::getNumNodes() const
{
    return numNodes;
}

inline dim_t NodeFile::getNumDegreesOfFreedom() const
{
    return dofDistribution->getMyNumComponents();
}

inline dim_t NodeFile::getNumDegreesOfFreedomTargets() const
{
    return degreesOfFreedomMapping.numTargets;
}

inline const index_t* NodeFile::borrowDegreesOfFreedomTarget() const
{
    return degreesOfFreedomMapping.map;
}

inline const index_t* NodeFile::borrowTargetDegreesOfFreedom() const
{
    return degreesOfFreedomMapping.target;
}

inline void NodeFile::updateTagList()
{
    util::setValuesInUse(Tag, numNodes, tagsInUse, mpiInfo);
}

}  // namespace shirley


#endif  // __SHIRLEY_NODEFILE_H__
