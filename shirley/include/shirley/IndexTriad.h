
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_NODETRIAD_H__
#define __SHIRLEY_NODETRIAD_H__


#include <escript/DataTypes.h>

#include <functional>

#include <boost/functional/hash.hpp>

namespace shirley {

using escript::DataTypes::index_t;

/**
 * IndexTriad is a simple class to hold three index_t variables.
 * It was first designed to hold the node IDs of the three vertices that
 * comprise the face of a triangle, in such a way that these IDs behaved
 * as a single entity that could be used as a key in a std::map or
 * std::unordered_map (see IndexTriadStdLess and IndexTriadHash to find
 * out more).
 */

class IndexTriad {
public:
    IndexTriad() :
        m_first(-1), m_second(-1), m_third(-1) { }

    IndexTriad(index_t first, index_t second, index_t third) :
        m_first(first), m_second(second), m_third(third) { }

    ~IndexTriad() = default;

    bool operator==(const IndexTriad& other) const;
    bool operator!=(const IndexTriad& other) const;

public:
    index_t m_first;

    index_t m_second;

    index_t m_third;
};

bool IndexTriad::operator==(const IndexTriad& other) const
{
    return (m_first == other.m_first) &&
        (m_second == other.m_second) &&
        (m_third == other.m_third);
}

bool IndexTriad::operator!=(const IndexTriad& other) const
{
    return !(operator==(other));
}

/**
 * IndexTriadStdLess is a simple class that allows us to check
 * whether one instance of IndexTriad is "less" than another instance
 * through the operator() member function of std::less. This design
 * enables IndexTriad to be used as keys in instances of std::map:
 *
 * std::map<IndexTriad, ???, IndexTriadStdLess> foo;
 *
 * See: https://stackoverflow.com/questions/1102392/stdmaps-with-user-defined-types-as-key
 */

class IndexTriadStdLess {
public:
    bool operator()(const IndexTriad& lhs, const IndexTriad& rhs) const
    {
        if (lhs.m_first < rhs.m_first) {
            return true;
        }

        if (lhs.m_first > rhs.m_first) {
            return false;
        }

        if (lhs.m_second < rhs.m_second) {
            return true;
        }

        if (lhs.m_second > rhs.m_second) {
            return false;
        }

        if (lhs.m_third < rhs.m_third) {
            return true;
        }

        if (lhs.m_third > rhs.m_third) {
            return false;
        }

        return false;
    }
};

/**
 * IndexTriadSHash is a simple class that allows us to hash an
 * instance of IndexTriad. This design enables IndexTriad to be used as
 * keys in instances of std::unordered_map:
 *
 * std::unordered_map<IndexTriad, ???, IndexTriadHash> foo;
 *
 * See: https://stackoverflow.com/questions/32685540/why-cant-i-compile-an-unordered-map-with-a-pair-as-key
 */

class IndexTriadHash {
public:
    std::size_t operator()(const IndexTriad &it) const {
        std::size_t seed = 0;
        boost::hash_combine(seed, it.m_first);
        boost::hash_combine(seed, it.m_second);
        boost::hash_combine(seed, it.m_third);

        return seed;
    }
};

}


#endif // __SHIRLEY_NODETRIAD_H__
