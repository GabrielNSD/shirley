
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/ShirleyDomain.h>
#include <shirley/ElementType.h>
#include <shirley/ElementFile.h>
#include <shirley/IndexTriad.h>
#include <shirley/LobattoPolynomials.h>

#include <escript/index.h>

#include <array>
#include <algorithm>
#include <utility>
#include <tuple>
#include <cmath>

#include <cstring>

#include <boost/functional/hash.hpp>
#include <boost/dynamic_bitset.hpp>
#include <tsl/robin_map.h>  // Faster replacement for std::unordered_map

#include <sys/time.h>

using escript::IOError;

namespace shirley {

static const int TriangleEdgeLayout[3][2] =
    { { 0, 1 }, { 1, 2 }, { 2, 0 } };

static const int TetrahedronEdgeLayout[6][2] =
    { { 0, 1 }, { 1, 2 }, { 2, 3 }, { 3, 0 }, { 0, 2 }, { 1, 3 } };

static const int TriangleFaceLayout[1][3] =
    { { 0, 1, 2 } };

static const int TetrahedronFaceLayout[4][3] =
    { { 0, 1, 2 }, { 1, 2, 3 }, { 2, 3, 0 }, { 0, 1, 3 } };

/**
 * Store in a map an adjacency between two nodes. Both the adjacency
 * from the first node to the second node as well as that from the
 * second node to the first node are stored. The caller may specify
 * whether they would like to check if each adjacency already exists
 * in the map.
 *
 * @param nodeAdjacencyMap
 * @param firstNode
 * @param secondNode
 * @param checkDuplicates
 */
static
void storeAdjacencies(
    tsl::robin_map<index_t, IndexVector> &nodeAdjacencyMap,
    index_t firstNode, index_t secondNode,
    bool checkDuplicates = true)
{
    // Store adjacency from the first node to the second node
    IndexVector& adjNodes1 = nodeAdjacencyMap[firstNode];

    if (checkDuplicates) {
        if (std::find(adjNodes1.begin(), adjNodes1.end(),
                      secondNode) == adjNodes1.end()) {
            adjNodes1.push_back(secondNode);
        }
    }
    else {
        adjNodes1.push_back(secondNode);
    }

    // Store adjacency from the second node to the first node
    IndexVector& adjNodes2 = nodeAdjacencyMap[secondNode];

    if (checkDuplicates) {
        if (std::find(adjNodes2.begin(), adjNodes2.end(),
                      firstNode) == adjNodes2.end()) {
            adjNodes2.push_back(firstNode);
        }
    }
    else {
        adjNodes2.push_back(firstNode);
    }
}

/**
 * For each element in an ElementFile, assign IDs to the SEM nodes
 * that should be located along element edges, inside element faces and
 * inside the element itself (in the 3D case).
 *
 * The function keeps track of which edges and faces have already been
 * processed through maps provided by the caller. The initial value that
 * should be used when assigning IDs to the SEM nodes must also be provided
 * by the caller through a reference, which gets updated so that the caller
 * may check how many IDs were created.
 *
 * An important side effect of this function is that all adjacencies
 * between vertex nodes, and also those between vertex nodes and the
 * generated SEM nodes, are stored in a map provided by the caller.
 *
 * @param elements
 * @param nodeAdjacencyMap
 * @param edgeInnerNodeMap
 * @param faceInnerNodeMap
 * @param new_nodeId
 */
static
void assignIdsToSemNodes(
    ElementFile *elements,
    tsl::robin_map<index_t, IndexVector>& nodeAdjacencyMap,
    tsl::robin_map<IndexPair, index_t,
        boost::hash<std::pair<index_t, index_t>>>& edgeInnerNodeMap,
    tsl::robin_map<IndexTriad, index_t, IndexTriadHash>& faceInnerNodeMap,
    index_t& new_nodeId)
{
    if ((elements->numElements < 1) ||
        (elements->m_numVerticesPerElem < 2)) {
        return;
    }

    index_t *elemNodes = elements->Nodes;
    bool hasInnerEdgeNodes =
        ((elements->m_numEdgesPerElem * elements->m_numInnerNodesPerEdge) > 0);
    bool hasInnerFaceNodes =
        ((elements->m_numFacesPerElem * elements->m_numInnerNodesPerFace) > 0);
    bool hasInteriorNodes = (elements->m_numInteriorNodesPerElem > 0);

    if (elements->elemTypeId == Shirley_Tri3) {
        index_t vertices[3];
        IndexPair edges[3];
        IndexTriad faces[1];

        int firstVertexIdx, secondVertexIdx, thirdVertexIdx;
        index_t vertexTriad[3];

        for (int el = 0; el < elements->numElements; ++el) {
            // Read vertices
            vertices[0] = elemNodes[elements->getVertexOffset(el, 0)];
            vertices[1] = elemNodes[elements->getVertexOffset(el, 1)];
            vertices[2] = elemNodes[elements->getVertexOffset(el, 2)];

            // Traverse edges (three, in this case)
            for (int ed = 0; ed < elements->m_numEdgesPerElem; ++ed) {
                firstVertexIdx = TriangleEdgeLayout[ed][0];
                secondVertexIdx = TriangleEdgeLayout[ed][1];

                edges[ed].first = std::min(
                    vertices[firstVertexIdx], vertices[secondVertexIdx]);
                edges[ed].second = std::max(
                    vertices[firstVertexIdx], vertices[secondVertexIdx]);

                // Store adjacency between edge vertices
                storeAdjacencies(nodeAdjacencyMap, edges[ed].first,
                    edges[ed].second, true);

                // Check and assign IDs for inner nodes in edges
                if (hasInnerEdgeNodes) {
                    // Check if the current edge has an initial node
                    // mapped to it
                    auto iter1 = edgeInnerNodeMap.find(edges[ed]);

                    if (iter1 != edgeInnerNodeMap.end()) {
                        // We have an initial node mapped to this edge,
                        // let's use it
                        int nodeStep =
                            (vertices[firstVertexIdx] < vertices[secondVertexIdx]) ?
                            +1 : -1;
                        index_t workNode = (nodeStep == +1) ? iter1->second :
                            (iter1->second + elements->m_numInnerNodesPerEdge - 1);
                        int nodeOffset = elements->getInnerEdgeNodeOffset(el, ed, 0);

                        for (int i = 0; i < elements->m_numInnerNodesPerEdge; ++i) {
                            elemNodes[nodeOffset] = workNode;
                            workNode += nodeStep;
                            ++nodeOffset;
                        }
                    }
                    else {
                        // There's no initial node mapped to this edge,
                        // let's assign it
                        edgeInnerNodeMap[edges[ed]] = new_nodeId;

                        int nodeStep =
                            (vertices[firstVertexIdx] < vertices[secondVertexIdx]) ?
                            +1 : -1;
                        index_t workNode = (nodeStep == +1) ? new_nodeId :
                            (new_nodeId + elements->m_numInnerNodesPerEdge - 1);
                        int nodeOffset = elements->getInnerEdgeNodeOffset(el, ed, 0);

                        // Store adjacency between first vertex and first inner node
                        storeAdjacencies(nodeAdjacencyMap, edges[ed].first,
                            new_nodeId, true);

                        for (int i = 0; i < elements->m_numInnerNodesPerEdge; ++i) {
                            elemNodes[nodeOffset] = workNode;

                            if (i > 0) {
                                // Store adjacency between current inner
                                // node and previous inner node
                                storeAdjacencies(nodeAdjacencyMap,
                                    workNode, workNode - nodeStep, true);
                            }

                            workNode += nodeStep;
                            ++nodeOffset;
                        }

                        // Store adjacency between last inner node and second vertex
                        storeAdjacencies(nodeAdjacencyMap, edges[ed].second,
                            new_nodeId + elements->m_numInnerNodesPerEdge - 1, true);

                        new_nodeId += elements->m_numInnerNodesPerEdge;
                    }
                }  // if (hasInnerEdgeNodes)
            }  // for each edge

            // Check and assign IDs for inner nodes in faces
            if (hasInnerFaceNodes) {
                // Traverse faces (just one, in this case)
                for (int f = 0; f < elements->m_numFacesPerElem; ++f) {
                    firstVertexIdx = TriangleFaceLayout[f][0];
                    secondVertexIdx = TriangleFaceLayout[f][1];
                    thirdVertexIdx = TriangleFaceLayout[f][2];

                    vertexTriad[0] = vertices[firstVertexIdx];
                    vertexTriad[1] = vertices[secondVertexIdx];
                    vertexTriad[2] = vertices[thirdVertexIdx];
                    std::sort(std::begin(vertexTriad), std::end(vertexTriad));

                    faces[f].m_first = vertexTriad[0];
                    faces[f].m_second = vertexTriad[1];
                    faces[f].m_third = vertexTriad[2];

                    // Check if the current face has an initial node
                    // mapped to it
                    auto iter1 = faceInnerNodeMap.find(faces[f]);

                    if (iter1 != faceInnerNodeMap.end()) {
                        // We have an initial node mapped to this face,
                        // let's use it
                        index_t workNode = iter1->second;
                        int nodeOffset = elements->getInnerFaceNodeOffset(el, f, 0);

                        for (int i = 0; i < elements->m_numInnerNodesPerFace; ++i) {
                            elemNodes[nodeOffset] = workNode;
                            ++workNode;
                            ++nodeOffset;
                        }
                    }
                    else {
                        // There's no initial node mapped to this face,
                        // let's assign it
                        faceInnerNodeMap[faces[f]] = new_nodeId;
                        int nodeOffset = elements->getInnerFaceNodeOffset(el, f, 0);

                        // Store adjacency between first vertex and first inner node
                        storeAdjacencies(nodeAdjacencyMap, faces[f].m_first,
                            new_nodeId, true);

                        for (int i = 0; i < elements->m_numInnerNodesPerFace; ++i) {
                            elemNodes[nodeOffset] = new_nodeId;

                            if (i > 0) {
                                // Store adjacency between current inner
                                // node and previous inner node
                                storeAdjacencies(nodeAdjacencyMap,
                                    new_nodeId, new_nodeId - 1, true);
                            }

                            ++new_nodeId;
                            ++nodeOffset;
                        }

                        // FIXME: Should we store an adjacency between the
                        //  last inner node and the second or third vertex?
                    }
                }  // for each face
            }  // if (hasInnerFaceNodes)
        }  // for each element

        return;
    }  // if (elements->elemTypeId == Shirley_Tri3)

    if (elements->elemTypeId == Shirley_Tet4) {
        index_t vertices[4];
        IndexPair edges[6];
        IndexTriad faces[4];

        int firstVertexIdx, secondVertexIdx, thirdVertexIdx;
        index_t vertexTriad[3];

        for (int el = 0; el < elements->numElements; ++el) {
            // Read vertices
            vertices[0] = elemNodes[elements->getVertexOffset(el, 0)];
            vertices[1] = elemNodes[elements->getVertexOffset(el, 1)];
            vertices[2] = elemNodes[elements->getVertexOffset(el, 2)];
            vertices[3] = elemNodes[elements->getVertexOffset(el, 3)];

            // Traverse edges (six, in this case)
            for (int ed = 0; ed < elements->m_numEdgesPerElem; ++ed) {
                firstVertexIdx = TetrahedronEdgeLayout[ed][0];
                secondVertexIdx = TetrahedronEdgeLayout[ed][1];

                edges[ed].first = std::min(
                    vertices[firstVertexIdx], vertices[secondVertexIdx]);
                edges[ed].second = std::max(
                    vertices[firstVertexIdx], vertices[secondVertexIdx]);

                // Store adjacency between edge vertices
                storeAdjacencies(nodeAdjacencyMap, edges[ed].first,
                    edges[ed].second, true);

                // Check and assign IDs for inner nodes in edges
                if (hasInnerEdgeNodes) {
                    // Check if the current edge has an initial node
                    // mapped to it
                    auto iter1 = edgeInnerNodeMap.find(edges[ed]);

                    if (iter1 != edgeInnerNodeMap.end()) {
                        // We have an initial node mapped to this edge,
                        // let's use it
                        int nodeStep =
                          (vertices[firstVertexIdx] < vertices[secondVertexIdx]) ?
                          +1 : -1;
                        index_t workNode = (nodeStep == +1) ? iter1->second :
                            (iter1->second + elements->m_numInnerNodesPerEdge - 1);
                        int nodeOffset = elements->getInnerEdgeNodeOffset(el, ed, 0);

                        for (int i = 0; i < elements->m_numInnerNodesPerEdge; ++i) {
                            elemNodes[nodeOffset] = workNode;
                            workNode += nodeStep;
                            ++nodeOffset;
                        }
                    }
                    else {
                        // There's no initial node mapped to this edge,
                        // let's assign it
                        edgeInnerNodeMap[edges[ed]] = new_nodeId;

                        int nodeStep =
                          (vertices[firstVertexIdx] < vertices[secondVertexIdx]) ?
                          +1 : -1;
                        index_t workNode = (nodeStep == +1) ? new_nodeId :
                            (new_nodeId + elements->m_numInnerNodesPerEdge - 1);
                        int nodeOffset = elements->getInnerEdgeNodeOffset(el, ed, 0);

                        // Store adjacency between first vertex and first inner node
                        storeAdjacencies(nodeAdjacencyMap, edges[ed].first,
                            new_nodeId, true);

                        for (int i = 0; i < elements->m_numInnerNodesPerEdge; ++i) {
                            elemNodes[nodeOffset] = workNode;

                            if (i > 0) {
                                // Store adjacency between current inner
                                // node and previous inner node
                                storeAdjacencies(nodeAdjacencyMap,
                                    workNode, workNode - nodeStep, true);
                            }

                            workNode += nodeStep;
                            ++nodeOffset;
                        }

                        // Store adjacency between last inner node and second vertex
                        storeAdjacencies(nodeAdjacencyMap, edges[ed].second,
                            new_nodeId + elements->m_numInnerNodesPerEdge - 1, true);

                        new_nodeId += elements->m_numInnerNodesPerEdge;
                    }
                }  // if (hasInnerEdgeNodes)
            }  // for each edge

            // Check and assign IDs for inner nodes in faces
            if (hasInnerFaceNodes) {
                // Traverse faces (four, in this case)
                for (int f = 0; f < elements->m_numFacesPerElem; ++f) {
                    firstVertexIdx = TetrahedronFaceLayout[f][0];
                    secondVertexIdx = TetrahedronFaceLayout[f][1];
                    thirdVertexIdx = TetrahedronFaceLayout[f][2];

                    vertexTriad[0] = vertices[firstVertexIdx];
                    vertexTriad[1] = vertices[secondVertexIdx];
                    vertexTriad[2] = vertices[thirdVertexIdx];
                    std::sort(std::begin(vertexTriad), std::end(vertexTriad));

                    faces[f].m_first = vertexTriad[0];
                    faces[f].m_second = vertexTriad[1];
                    faces[f].m_third = vertexTriad[2];

                    // Check if the current face has an initial node
                    // mapped to it
                    auto iter1 = faceInnerNodeMap.find(faces[f]);

                    if (iter1 != faceInnerNodeMap.end()) {
                        // We have an initial node mapped to this face,
                        // let's use it
                        index_t workNode = iter1->second;
                        int nodeOffset = elements->getInnerFaceNodeOffset(el, f, 0);

                        for (int i = 0; i < elements->m_numInnerNodesPerFace; ++i) {
                            elemNodes[nodeOffset] = workNode;
                            ++workNode;
                            ++nodeOffset;
                        }
                    }
                    else {
                        // There's no initial node mapped to this face,
                        // let's assign it
                        faceInnerNodeMap[faces[f]] = new_nodeId;
                        int nodeOffset = elements->getInnerFaceNodeOffset(el, f, 0);

                        // Store adjacency between first vertex and first inner node
                        storeAdjacencies(nodeAdjacencyMap, faces[f].m_first,
                            new_nodeId, true);

                        for (int i = 0; i < elements->m_numInnerNodesPerFace; ++i) {
                            elemNodes[nodeOffset] = new_nodeId;

                            if (i > 0) {
                                // Store adjacency between current inner
                                // node and previous inner node
                                storeAdjacencies(nodeAdjacencyMap,
                                    new_nodeId, new_nodeId - 1, true);
                            }

                            ++new_nodeId;
                            ++nodeOffset;
                        }

                        // FIXME: Should we store an adjacency between the
                        //  last inner node and the last vertex?
                    }
                }  // for each face
            }  // if (hasInnerFaceNodes)

            // Assign IDs for interior nodes
            if (hasInteriorNodes) {
                // Interior nodes are not shared between elements,
                // so there's no need to check if IDs have been
                // assigned already
                int nodeOffset = elements->getInteriorNodeOffset(el, 0);

                // Store adjacency between first vertex and first interior node
                std::sort(std::begin(vertices), std::end(vertices));
                storeAdjacencies(nodeAdjacencyMap, vertices[0],
                    new_nodeId, true);

                for (int i = 0; i < elements->m_numInteriorNodesPerElem; ++i) {
                    elemNodes[nodeOffset] = new_nodeId;

                    if (i > 0) {
                        // Store adjacency between current interior
                        // node and previous interior node
                        storeAdjacencies(nodeAdjacencyMap,
                            new_nodeId, new_nodeId - 1, true);
                    }

                    ++new_nodeId;
                    ++nodeOffset;
                }

                // FIXME: Should we store an adjacency between the
                //  last interior node and the last vertex?
            }
        }  // for each element

        return;
    }  // if (elements->elemTypeId == Shirley_Tet4)
}

/**
 * For an instance of ElementFile containing triangular elements, ensures
 * that the vertices of all triangles are stored in counter-clockwise order.
 * This is necessary if one wants to be able to transform these triangles
 * to a standard triangular element T_st through nodal shape functions later,
 * as these shape functions require triangle vertices to be in
 * counter-clockwise order.
 *
 * @param elements
 * @param domainNodes
 * @param numDim
 * @param nodeIdToNodeFileArrayMap
 */
static
void ensureCounterClockwiseTrianglesIn2D(
    ElementFile *elements, NodeFile *domainNodes, int numDim,
    const tsl::robin_map<index_t, index_t> &nodeIdToNodeFileArrayMap)
{
    // Triangles only, please
    if (elements->m_numVerticesPerElem != 3) {
        return;
    }

    index_t *elemNodes = elements->Nodes;

#pragma omp parallel for
    for (int el = 0; el < elements->numElements; ++el) {
        // Read vertex node IDs for this element and fetch their coordinates
        index_t vertices[3];

        vertices[0] = elemNodes[elements->getVertexOffset(el, 0)];
        vertices[1] = elemNodes[elements->getVertexOffset(el, 1)];
        vertices[2] = elemNodes[elements->getVertexOffset(el, 2)];

        real_t x[3], y[3];  // No z-coordinate, 2D support only

        index_t arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[0]);
        x[0] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[0] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[1]);
        x[1] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[1] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[2]);
        x[2] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[2] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        // Compute the area of the triangle. If it is positive, the
        // vertices are in clockwise order. See:
        //   https://stackoverflow.com/a/1165943/10453647
        const double doubleArea = ((x[1] - x[0]) * (y[1] + y[0]))
            + ((x[2] - x[1]) * (y[2] + y[1]))
            + ((x[0] - x[2]) * (y[0] + y[2]));

        if (doubleArea >= 0.0) {
            // Switch second and third vertices so that they are
            // in counter-clockwise order
            elemNodes[elements->getVertexOffset(el, 1)] = vertices[2];
            elemNodes[elements->getVertexOffset(el, 2)] = vertices[1];
        }
    }
}

inline static
bool valuesAreClose(double x, double y, double tolerance)
{
    return fabs(x - y) <= tolerance;
}

/**
 * For an instance of ElementFile containing triangular elements, computes
 * the coordinates of the SEM nodes located over the edges of the elements
 * or inside the elements.
 *
 * @param elements
 * @param domainNodes
 * @param numDim
 * @param nodeIdToNodeFileArrayMap
 * @param nodeAdjacencyMap
 * @param edgeInnerNodeMap
 * @param faceInnerNodeMap
 * @param xiEtaPairs
 */
static
void computeCoordinatesForSemNodes(
    ElementFile *elements, NodeFile* domainNodes, int numDim,
    const tsl::robin_map<index_t, index_t>& nodeIdToNodeFileArrayMap,
    const tsl::robin_map<IndexPair, index_t,
      boost::hash<std::pair<index_t, index_t>>>& edgeInnerNodeMap,
    const tsl::robin_map<IndexTriad, index_t, IndexTriadHash>& faceInnerNodeMap,
    const std::vector<std::pair<double, double>>& xiEtaPairs)
{
    // Triangles only, please
    if (elements->m_numVerticesPerElem != 3) {
        return;
    }

    index_t *elemNodes = elements->Nodes;

    index_t vertices[3];
    IndexPair edges[3];
    IndexTriad faces[1];

    std::array<std::vector<index_t>, 3> semNodesOnEdges;
    std::array<std::vector<index_t>, 1> semNodesOnFaces;

    index_t arrayIdx;
    real_t x[3], y[3];  // No z-coordinate, 2D support only
    int firstVertexIdx, secondVertexIdx, thirdVertexIdx;
    int nodeStep[3];
    index_t vertexTriad[3];

    std::array<int, 3 + 1> idxForSemNodeVectors =
        { -1, -1, -1, -1 };  // 3 edges + 1 face

    for (int el = 0; el < elements->numElements; ++el) {
        // Read vertex node IDs for this element and fetch their coordinates.
        // We assume the coordinates of the vertices are in counter-clockwise
        // order (please see the ensureCounterClockwiseTrianglesIn2D()
        // function for reference).
        vertices[0] = elemNodes[elements->getVertexOffset(el, 0)];
        vertices[1] = elemNodes[elements->getVertexOffset(el, 1)];
        vertices[2] = elemNodes[elements->getVertexOffset(el, 2)];

        arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[0]);
        x[0] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[0] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[1]);
        x[1] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[1] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[2]);
        x[2] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[2] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        // Assemble edges.
        // First edge => (x[0],y[0]) to (x[1],y[1])
        firstVertexIdx = 0;
        secondVertexIdx = 1;

        edges[0].first = std::min(
            vertices[firstVertexIdx], vertices[secondVertexIdx]);
        edges[0].second = std::max(
            vertices[firstVertexIdx], vertices[secondVertexIdx]);
        nodeStep[0] =
            (vertices[firstVertexIdx] < vertices[secondVertexIdx]) ?
            +1 : -1;

        // Second edge => (x[0],y[0]) to (x[2],y[2])
        firstVertexIdx = 0;
        secondVertexIdx = 2;

        edges[1].first = std::min(
            vertices[firstVertexIdx], vertices[secondVertexIdx]);
        edges[1].second = std::max(
            vertices[firstVertexIdx], vertices[secondVertexIdx]);
        nodeStep[1] =
            (vertices[firstVertexIdx] < vertices[secondVertexIdx]) ?
            +1 : -1;

        // Third edge => (x[1],y[1]) to (x[2],y[2])
        firstVertexIdx = 1;
        secondVertexIdx = 2;

        edges[2].first = std::min(
            vertices[firstVertexIdx], vertices[secondVertexIdx]);
        edges[2].second = std::max(
            vertices[firstVertexIdx], vertices[secondVertexIdx]);
        nodeStep[2] =
            (vertices[firstVertexIdx] < vertices[secondVertexIdx]) ?
            +1 : -1;

        // Gathers the IDs of the SEM nodes for all edges.
        // First edge
        auto iter1 = edgeInnerNodeMap.find(edges[0]);

        if (iter1 == edgeInnerNodeMap.end()) {
            // FIXME: Maybe we should throw an exception here?
            continue;
        }

        index_t semNode = iter1->second;
        index_t workNode = (nodeStep[0] == +1) ? semNode :
            (semNode + elements->m_numInnerNodesPerEdge - 1);

        semNodesOnEdges[0].clear();

        for (int i = 0; i < elements->m_numInnerNodesPerEdge; ++i) {
            semNodesOnEdges[0].push_back(workNode);
            workNode += nodeStep[0];
        }

        // Second edge
        iter1 = edgeInnerNodeMap.find(edges[1]);

        if (iter1 == edgeInnerNodeMap.end()) {
            // FIXME: Maybe we should throw an exception here?
            continue;
        }

        semNode = iter1->second;
        workNode = (nodeStep[1] == +1) ? semNode :
            (semNode + elements->m_numInnerNodesPerEdge - 1);

        semNodesOnEdges[1].clear();

        for (int i = 0; i < elements->m_numInnerNodesPerEdge; ++i) {
            semNodesOnEdges[1].push_back(workNode);
            workNode += nodeStep[1];
        }

        // Third edge
        iter1 = edgeInnerNodeMap.find(edges[2]);

        if (iter1 == edgeInnerNodeMap.end()) {
            // FIXME: Maybe we should throw an exception here?
            continue;
        }

        semNode = iter1->second;
        workNode = (nodeStep[2] == +1) ? semNode :
            (semNode + elements->m_numInnerNodesPerEdge - 1);

        semNodesOnEdges[2].clear();

        for (int i = 0; i < elements->m_numInnerNodesPerEdge; ++i) {
            semNodesOnEdges[2].push_back(workNode);
            workNode += nodeStep[2];
        }

        // Gathers the IDs of the SEM nodes for the sole face
        firstVertexIdx = 0;
        secondVertexIdx = 1;
        thirdVertexIdx = 2;

        vertexTriad[0] = vertices[firstVertexIdx];
        vertexTriad[1] = vertices[secondVertexIdx];
        vertexTriad[2] = vertices[thirdVertexIdx];
        std::sort(std::begin(vertexTriad), std::end(vertexTriad));

        faces[0].m_first = vertexTriad[0];
        faces[0].m_second = vertexTriad[1];
        faces[0].m_third = vertexTriad[2];

        auto iter2 = faceInnerNodeMap.find(faces[0]);

        if (iter2 == faceInnerNodeMap.end()) {
            // FIXME: Maybe we should throw an exception here?
            continue;
        }

        workNode = iter2->second;

        semNodesOnFaces[0].clear();

        for (int i = 0; i < elements->m_numInnerNodesPerFace; ++i) {
            semNodesOnFaces[0].push_back(workNode);
            ++workNode;
        }

        // For each pair of xi/eta coordinates over the standard triangle,
        // combines xi/eta to the coordinates of the vertices to compute
        // the coordinates of the SEM nodes in the triangle of the element,
        // and stores the computed coordinates under the correct offset
        // in the node file. We assume the xi/eta pairs are in ascending
        // order, with xi as the first sort criterion and eta the second.
        double xi, eta;
        bool pointIsOverAnEdge;
        double calcX, calcY;
        u_char nodeType;

        idxForSemNodeVectors.fill(-1);

        for (const auto & xiEtaPair : xiEtaPairs) {
            xi = xiEtaPair.first;
            eta = xiEtaPair.second;

            calcX = LobattoPolynomials::calcXFromStandardToGeneralTriangle(
                xi, eta, x[0], x[1], x[2]);
            calcY = LobattoPolynomials::calcYFromStandardToGeneralTriangle(
                xi, eta, y[0], y[1], y[2]);

            pointIsOverAnEdge = false;
            arrayIdx = -1;
            nodeType = NodeType_Unknown;

            // First edge => (x[0],y[0]) to (x[1],y[1]).
            // Horizontal segment from (xi=0,eta=0) to (xi=1,eta=0)
            if (valuesAreClose(eta, 0.0, 1E-6)) {
                pointIsOverAnEdge = true;

                if (valuesAreClose(xi, 0.0, 1E-6) ||
                    valuesAreClose(xi, 1.0, 1E-6)) {
                    // Nothing to do, we're over a vertex
                }
                else {
                    // We're over an inner SEM node on this edge
                    ++idxForSemNodeVectors[0];
                    arrayIdx = nodeIdToNodeFileArrayMap.at(
                        semNodesOnEdges[0][idxForSemNodeVectors[0]]);
                    nodeType = NodeType_SemNodeOverAnEdge;
                }
            }

            // Second edge => (x[0],y[0]) to (x[2],y[2]).
            // Vertical segment from (xi=0,eta=0) to (xi=0,eta=1)
            if (valuesAreClose(xi, 0.0, 1E-6)) {
                pointIsOverAnEdge = true;

                if (valuesAreClose(eta, 0.0, 1E-6) ||
                    valuesAreClose(eta, 1.0, 1E-6)) {
                    // Nothing to do, we're over a vertex
                }
                else {
                    // We're over an inner SEM node on this edge
                    ++idxForSemNodeVectors[1];
                    arrayIdx = nodeIdToNodeFileArrayMap.at(
                        semNodesOnEdges[1][idxForSemNodeVectors[1]]);
                    nodeType = NodeType_SemNodeOverAnEdge;
                }
            }

            // Third edge => (x[1],y[1]) to (x[2],y[2]).
            // Diagonal segment from (xi=0,eta=1) to (xi=1,eta=0)
            if (valuesAreClose(xi + eta, 1.0, 1E-6)) {
                pointIsOverAnEdge = true;

                if (valuesAreClose(eta, 1.0, 1E-6) ||
                    valuesAreClose(xi,  1.0, 1E-6)) {
                    // Nothing to do, we're over a vertex
                }
                else {
                    // We're over an inner SEM node on this edge
                    ++idxForSemNodeVectors[2];
                    arrayIdx = nodeIdToNodeFileArrayMap.at(
                        semNodesOnEdges[2][idxForSemNodeVectors[2]]);
                    nodeType = NodeType_SemNodeOverAnEdge;
                }
            }

            // Inner SEM nodes over the sole face
            if (!pointIsOverAnEdge) {
                ++idxForSemNodeVectors[3];
                arrayIdx = nodeIdToNodeFileArrayMap.at(
                    semNodesOnFaces[0][idxForSemNodeVectors[3]]);
                nodeType = NodeType_SemNodeOverAFace;
            }

            // Stores the computed coordinates of the SEM node under the
            // correct offset in the node file
            if (arrayIdx != -1) {
                domainNodes->Coordinates[
                    INDEX2(0, arrayIdx, numDim)] = calcX;
                domainNodes->Coordinates[
                    INDEX2(1, arrayIdx, numDim)] = calcY;

                if (numDim > 2) {
                    // We do not calculate z-coordinates yet,
                    //  2D support only
                    domainNodes->Coordinates[
                        INDEX2(2, arrayIdx, numDim)] = 0.0;
                }

                if (nodeType != NodeType_Unknown) {
                    domainNodes->m_nodeType[arrayIdx] = nodeType;
                }
            }
        }
    }
}

static
std::tuple<double, double, double, double, index_t, index_t>
    getMinMaxCoordsAndNodes(
        ElementFile *elements, NodeFile *domainNodes, int numDim,
        const tsl::robin_map<index_t, index_t> &nodeIdToNodeFileArrayMap)
{
    // Triangles only, please
    if (elements->m_numVerticesPerElem != 3) {
        return std::make_tuple(0.0, 0.0, 0.0, 0.0, 0, 0);
    }

    index_t *elemNodes = elements->Nodes;

    double min_x = std::numeric_limits<double>::max();
    double min_y = std::numeric_limits<double>::max();
    double max_x = std::numeric_limits<double>::lowest();
    double max_y = std::numeric_limits<double>::lowest();
    index_t min_coords_node = std::numeric_limits<index_t>::max();
    index_t max_coords_node = std::numeric_limits<index_t>::lowest();

    for (int el = 0; el < elements->numElements; ++el) {
        // Read vertex node IDs for this element and fetch their coordinates
        index_t vertices[3];

        vertices[0] = elemNodes[elements->getVertexOffset(el, 0)];
        vertices[1] = elemNodes[elements->getVertexOffset(el, 1)];
        vertices[2] = elemNodes[elements->getVertexOffset(el, 2)];

        real_t x[3], y[3];  // No z-coordinate, 2D support only

        index_t arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[0]);
        x[0] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[0] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[1]);
        x[1] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[1] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[2]);
        x[2] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[2] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        // Store minimum and maximum coordinates
        min_x = std::min({ min_x, x[0], x[1], x[2] });
        min_y = std::min({ min_y, y[0], y[1], y[2] });
        max_x = std::max({ max_x, x[0], x[1], x[2] });
        max_y = std::max({ max_y, y[0], y[1], y[2] });

        // Store nodes with minimum coordinates
        if ( valuesAreClose(min_x, x[0], 1E-5) &&
             valuesAreClose(min_y, y[0], 1E-5) ) {
            min_coords_node = vertices[0];
        }
        else if (
              valuesAreClose(min_x, x[1], 1E-5) &&
              valuesAreClose(min_y, y[1], 1E-5) ) {
            min_coords_node = vertices[1];
        }
        else if (
              valuesAreClose(min_x, x[2], 1E-5) &&
              valuesAreClose(min_y, y[2], 1E-5) ) {
            min_coords_node = vertices[2];
        }

        // Store nodes with maximum coordinates
        if ( valuesAreClose(max_x, x[0], 1E-5) &&
             valuesAreClose(max_y, y[0], 1E-5) ) {
            max_coords_node = vertices[0];
        }
        else if (
              valuesAreClose(max_x, x[1], 1E-5) &&
              valuesAreClose(max_y, y[1], 1E-5) ) {
            max_coords_node = vertices[1];
        }
        else if (
              valuesAreClose(max_x, x[2], 1E-5) &&
              valuesAreClose(max_y, y[2], 1E-5) ) {
            max_coords_node = vertices[2];
        }
    }

    return std::make_tuple(min_x, min_y, max_x, max_y,
        min_coords_node, max_coords_node);
}

static
void computeCentroidVector(
    ElementFile *elements, NodeFile *domainNodes, int numDim,
    const tsl::robin_map<index_t, index_t> &nodeIdToNodeFileArrayMap,
    std::vector<std::tuple<int, double, double>> &elemIdxCentroidVec)
{
    // Triangles only, please
    if (elements->m_numVerticesPerElem != 3) {
        return;
    }

    index_t *elemNodes = elements->Nodes;

    elemIdxCentroidVec.clear();

    for (int el = 0; el < elements->numElements; ++el) {
        // Read vertex node IDs for this element and fetch their coordinates
        index_t vertices[3];

        vertices[0] = elemNodes[elements->getVertexOffset(el, 0)];
        vertices[1] = elemNodes[elements->getVertexOffset(el, 1)];
        vertices[2] = elemNodes[elements->getVertexOffset(el, 2)];

        real_t x[3], y[3];  // No z-coordinate, 2D support only

        index_t arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[0]);
        x[0] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[0] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[1]);
        x[1] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[1] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        arrayIdx = nodeIdToNodeFileArrayMap.at(vertices[2]);
        x[2] = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
        y[2] = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];

        double x_centroid = (x[0] + x[1] + x[2]) / 3.0;
        double y_centroid = (y[0] + y[1] + y[2]) / 3.0;

        elemIdxCentroidVec.emplace_back(el, x_centroid, y_centroid);
    }
}

static
void computeDistToRefAtNodes(
    ElementFile *elements, NodeFile *domainNodes, int numDim,
    double refPointX, double refPointY,
    const tsl::robin_map<index_t, index_t> &nodeIdToNodeFileArrayMap,
    tsl::robin_map<int, std::tuple<double, double, double>>
        &nodeIdToDistToRefMap)
{
    // Triangles only, please
    if (elements->m_numVerticesPerElem != 3) {
        return;
    }

    index_t *elemNodes = elements->Nodes;
    int numTotalNodesPerElem = elements->m_numTotalNodesPerElem;

    nodeIdToDistToRefMap.clear();

    for (int el = 0; el < elements->numElements; ++el) {
        for (int p = 0; p < numTotalNodesPerElem; ++p) {
            // Get the (x,y) coordinates of the vertex nodes for this element
            index_t nodeId = elemNodes[elements->getVertexOffset(el, p)];

            if (nodeIdToDistToRefMap.find(nodeId) == nodeIdToDistToRefMap.end()) {
                index_t arrayIdx = nodeIdToNodeFileArrayMap.at(nodeId);
                real_t x = domainNodes->Coordinates[INDEX2(0, arrayIdx, numDim)];
                real_t y = domainNodes->Coordinates[INDEX2(1, arrayIdx, numDim)];
                double distToRef = std::sqrt(
                    std::pow(x - refPointX, 2.0) + std::pow(y - refPointY, 2.0));

                nodeIdToDistToRefMap[nodeId] = std::make_tuple(distToRef, x, y);
            }
        }
    }
}

static
void fillNodeIdToElemIndexMap(
    ElementFile *elements, NodeFile *domainNodes, int numDim,
    tsl::robin_map<int, std::vector<int>> &nodeIdToElemIndexMap)
{
    // Triangles only, please
    if (elements->m_numVerticesPerElem != 3) {
        return;
    }

    index_t *elemNodes = elements->Nodes;
    int numTotalNodesPerElem = elements->m_numTotalNodesPerElem;

    nodeIdToElemIndexMap.clear();

    for (int el = 0; el < elements->numElements; ++el) {
        for (int p = 0; p < numTotalNodesPerElem; ++p) {
            index_t nodeId = elemNodes[elements->getVertexOffset(el, p)];

            std::vector<int> &elemIndices = nodeIdToElemIndexMap[nodeId];
            // FIXME Check for duplicates?
            elemIndices.push_back(el);
        }
    }
}

static inline
long sgn(long x)
{
    if (x > 0) return +1;
    if (x < 0) return -1;
    return 0;
}

/**
 * Generalized Hilbert ('gilbert') space-filling curve for arbitrary-sized
 * 2D rectangular grids. See:
 *   https://github.com/jakubcerveny/gilbert/blob/master/gilbert2d.py
 */
static
void gilbert2d_vector(
    long x, long y, const long ax, const long ay, const long bx, const long by,
    std::vector<std::pair<double, double>> &dest, int &maxDepth)
{
    long w = std::abs(ax + ay);
    long h = std::abs(bx + by);

    long dax = sgn(ax);  // unit major direction
    long day = sgn(ay);  // unit major direction
    long dbx = sgn(bx);  // unit orthogonal direction
    long dby = sgn(by);  // unit orthogonal direction

    if (h == 1) {
        // trivial row fill
        for (int i = 0; i < w; ++i) {
            dest.emplace_back(x, y);
            x = x + dax;
            y = y + day;
        }

        return;
    }

    if (w == 1) {
        // trivial column fill
        for (int i = 0; i < h; ++i) {
            dest.emplace_back(x, y);
            x = x + dbx;
            y = y + dby;
        }

        return;
    }

    long ax2 = ax / 2;
    long ay2 = ay / 2;
    long bx2 = bx / 2;
    long by2 = by / 2;

    long w2 = std::abs(ax2 + ay2);
    long h2 = std::abs(bx2 + by2);

    if ((2 * w) > (3 * h)) {
        if ((w2 % 2 != 0) && (w > 2)) {
            // prefer even steps
            ax2 = ax2 + dax;
            ay2 = ay2 + day;
        }

        // long case: split in two parts only
        int maxDepth_1 = maxDepth + 1;
        int maxDepth_2 = maxDepth + 1;
        gilbert2d_vector(x, y, ax2, ay2, bx, by, dest, maxDepth_1);
        gilbert2d_vector(x + ax2, y + ay2, ax - ax2, ay - ay2, bx, by, dest, maxDepth_2);
        maxDepth = std::max(maxDepth_1, maxDepth_2);
    }
    else {
        if ((h2 % 2 != 0) and (h > 2)) {
            // prefer even steps
            bx2 = bx2 + dbx;
            by2 = by2 + dby;
        }

        // standard case: one step up, one long horizontal, one step down
        int maxDepth_1 = maxDepth + 1;
        int maxDepth_2 = maxDepth + 1;
        int maxDepth_3 = maxDepth + 1;
        gilbert2d_vector(x, y, bx2, by2, ax2, ay2, dest, maxDepth_1);
        gilbert2d_vector(x + bx2, y + by2, ax, ay, bx - bx2, by - by2, dest, maxDepth_2);
        gilbert2d_vector(x + (ax - dax) + (bx2 - dbx),
                         y + (ay - day) + (by2 - dby),
                         -bx2, -by2, -(ax - ax2), -(ay - ay2), dest, maxDepth_3);
        maxDepth = std::max({ maxDepth_1, maxDepth_2, maxDepth_3 });
    }
}

#define MAX_NUM_NODES_GMSH 20

#define READ_GMSH_DEBUG

/// Reads a mesh from a gmsh file of name filename
escript::Domain_ptr ShirleyDomain::readGmsh(
    escript::JMPI mpiInfo,const std::string& filename,
    int numDim, int order, bool optimize, int traversalStrategy)
{
    if (mpiInfo->size > 1) {
        throw ShirleyException(
            "ShirleyDomain::readGmsh(): reading gmsh with MPI is not supported.");
    }

    if ((order < 3) || (order > 10)) {
        throw ShirleyException(
            "ShirleyDomain::readGmsh(): element order with respect to SEM "
            "must be >= 3 and <= 10.");
    }

    // Allocate domain
    ShirleyDomain* domain = new ShirleyDomain(filename, numDim, mpiInfo);

    // Open file
    std::ifstream file(filename);

    if (!file.good()) {
        delete domain;

        std::stringstream ss;
        ss << "ShirleyDomain::readGmsh(): opening gmsh file " << filename
           << " for reading failed.";
        throw IOError(ss.str());
    }

    // Start reading. For the file format description, see:
    // http://gmsh.info/doc/texinfo/gmsh.html#MSH-file-format-version-1
    // http://gmsh.info/doc/texinfo/gmsh.html#MSH-file-format-version-2
    // http://gmsh.info/doc/texinfo/gmsh.html#MSH-file-format-_0028version-4_0029
    std::string line;
    double fileVersion = 1.0;

    index_t min_nodeId = escript::DataTypes::index_t_max();
    index_t max_nodeId = escript::DataTypes::index_t_min();

    index_t min_vertexId = min_nodeId;
    index_t max_vertexId = max_nodeId;

    while (true) {
        // Find line starting with $
        do {
            std::getline(file, line);

            if (!file.good()) {
                break;
            }
        } while (line[0] != '$');

        if (file.eof()) {
            break;
        }

        // Read file format
        if (line.substr(1, 10) == "MeshFormat") {
            std::getline(file, line);

            if (file.eof()) {
                delete domain;
                throw IOError(
                    "ShirleyDomain::readGmsh(): early EOF while reading file");
            }

            int fileType = 0, dataSize = sizeof(double);

            std::stringstream ss(line);
            ss >> fileVersion >> fileType >> dataSize;
        } else if (line.substr(1, 3) == "NOD" ||
                   line.substr(1, 3) == "NOE" ||
                   line.substr(1, 5) == "Nodes") {
            // Read nodes
            std::getline(file, line);

            if (file.eof()) {
                delete domain;
                throw IOError(
                    "ShirleyDomain::readGmsh(): early EOF while reading file");
            }

            dim_t totalNumNodes = std::stoi(line);
            NodeFile* domNodes = domain->getNodes();
            domNodes->allocTable(totalNumNodes);

            for (index_t n = 0; n < totalNumNodes; ++n) {
                std::getline(file, line);

                if (!file.good()) {
                    delete domain;
                    throw IOError(
                        "ShirleyDomain::readGmsh(): early EOF while reading file");
                }

                std::stringstream ss(line);
                ss >> domNodes->Id[n]
                   >> domNodes->Coordinates[INDEX2(0, n, numDim)];

                if (numDim > 1) {
                    ss >> domNodes->Coordinates[INDEX2(1, n, numDim)];
                }

                if (numDim > 2) {
                    ss >> domNodes->Coordinates[INDEX2(2, n, numDim)];
                }

                domNodes->globalDegreesOfFreedom[n] = domNodes->Id[n];
                domNodes->Tag[n] = 0;
                domNodes->m_nodeType[n] = NodeType_Vertex;

                min_nodeId = std::min(min_nodeId, domNodes->Id[n]);
                max_nodeId = std::max(max_nodeId, domNodes->Id[n]);
            }
        } else if (line.substr(1, 3) == "ELM" ||
                   line.substr(1, 8) == "Elements") {
            // Read elements
            std::getline(file, line);

            if (file.eof()) {
                delete domain;
                throw IOError(
                    "ShirleyDomain::readGmsh(): early EOF while reading file");
            }

            dim_t totalNumElements = std::stoi(line);

            unsigned long totalNumElements_ul =
                static_cast<unsigned long>(totalNumElements);  // Make clang-tidy happy

            IndexVector id(totalNumElements_ul);
            std::vector<ElementTypeId> elementTypeId(totalNumElements_ul);
            IntVector tag(totalNumElements_ul);
            IndexVector vertices(totalNumElements_ul * MAX_NUM_NODES_GMSH);

            ElementTypeId finalElementTypeId = Shirley_NoRef;
            ElementTypeId finalFaceElementTypeId = Shirley_NoRef;
            index_t numElements = 0;
            index_t numFaceElements = 0;

            // Read all in
            for (index_t e = 0; e < totalNumElements; ++e) {
                std::getline(file, line);

                if (file.eof()) {
                    delete domain;
                    throw IOError(
                        "ShirleyDomain::readGmsh(): early EOF while reading file");
                }

                int gmshType;

                std::stringstream ss(line);
                ss >> id[e] >> gmshType;

                int elementDim = 0, numVerticesPerElem1 = 0;

                switch (gmshType) {
                    // Unsupported type
                    /*
                    case 1:  // Line order 1
                        elementTypeId[e] = Shirley_Line2;
                        elementDim = 1;
                        numVerticesPerElem1 = 2;
                        break;
                    */

                    case 2:  // Triangle order 1
                        elementTypeId[e] = Shirley_Tri3;
                        numVerticesPerElem1 = 3;
                        elementDim = 2;
                        break;

                    case 4:  // Tetrahedron order 1
                        elementTypeId[e] = Shirley_Tet4;
                        numVerticesPerElem1 = 4;
                        elementDim = 3;
                        break;

                    // Unsupported type
                    /*
                    case 15:  // Point
                        elementTypeId[e] = Shirley_Point1;
                        numVerticesPerElem1 = 1;
                        elementDim = 0;
                        break;
                    */

                    default: {
                        delete domain;

                        std::stringstream ss2;
                        ss2 << "ShirleyDomain::readGmsh(): unexpected gmsh "
                               "element type " << gmshType
                            << " in mesh file " << filename;
                        throw IOError(ss2.str());
                    }
                }

                if (elementDim == numDim) {
                    if (finalElementTypeId == Shirley_NoRef) {
                        finalElementTypeId = elementTypeId[e];
                    } else if (finalElementTypeId != elementTypeId[e]) {
                        delete domain;
                        throw IOError("ShirleyDomain::readGmsh(): Shirley can "
                                      "handle a single type of internal elements only.");
                    }

                    ++numElements;
                } else if (elementDim == numDim - 1) {
                    if (finalFaceElementTypeId == Shirley_NoRef) {
                        finalFaceElementTypeId = elementTypeId[e];
                    } else if (finalFaceElementTypeId != elementTypeId[e]) {
                        delete domain;
                        throw IOError("ShirleyDomain::readGmsh(): Shirley can "
                                      "handle a single type of face elements only.");
                    }

                    ++numFaceElements;
                }

                int elementaryId, partitionId;
                int numVerticesPerElem2;

                if (fileVersion <= 1.0) {
                    ss >> tag[e] >> elementaryId >> numVerticesPerElem2;
                    partitionId = 1;

                    if (numVerticesPerElem2 != numVerticesPerElem1) {
                        delete domain;

                        std::stringstream ss2;
                        ss2 << "ShirleyDomain::readGmsh(): illegal number "
                               "of nodes for element " << id[e]
                            << " in mesh file " << filename;
                        throw IOError(ss2.str());
                    }
                } else {
                    int numTags;

                    ss >> numTags;
                    tag[e] = elementaryId = partitionId = 1;
                    numVerticesPerElem2 = -1;

                    int itmp;

                    for (int j = 0; j < numTags; ++j) {
                        ss >> itmp;

                        if (j == 0) {
                            tag[e] = itmp;
                        } else if (j == 1) {
                            elementaryId = itmp;
                        } else if (j == 2) {
                            partitionId = itmp;
                        }
                        // Ignore any other tags
                    }
                }

                index_t vertexId;

                for (int n = 0; n < numVerticesPerElem1; ++n) {
                    ss >> vertexId;
                    vertices[INDEX2(n, e, MAX_NUM_NODES_GMSH)] = vertexId;

                    min_vertexId = std::min(min_vertexId, vertexId);
                    max_vertexId = std::max(max_vertexId, vertexId);
                }
            }

            // All elements have been read, now we have to identify the
            // shirley elements to define Elements and FaceElements
            if (finalElementTypeId == Shirley_NoRef) {
                if (numDim == 1) {
                    finalElementTypeId = Shirley_Line2;
                } else if (numDim == 2) {
                    finalElementTypeId = Shirley_Tri3;
                } else if (numDim == 3) {
                    finalElementTypeId = Shirley_Tet4;
                }
            }

            if (finalFaceElementTypeId == Shirley_NoRef) {
                if (numDim == 1) {
                    finalFaceElementTypeId = Shirley_Point1;
                } else if (numDim == 2) {
                    finalFaceElementTypeId = Shirley_Line2;
                } else if (numDim == 3) {
                    finalFaceElementTypeId = Shirley_Tri3;
                }
            }

            ElementFile* elements = new ElementFile(
                finalElementTypeId, mpiInfo, order);
            domain->setElements(elements);

            ElementFile* faces = new ElementFile(
                finalFaceElementTypeId, mpiInfo, order);
            domain->setFaceElements(faces);

            ElementFile* points = new ElementFile(
                Shirley_Point1, mpiInfo, order);
            domain->setPoints(points);

            elements->allocTable(numElements);
            faces->allocTable(numFaceElements);
            points->allocTable(0);

            elements->minColor = 0;
            elements->maxColor = numElements - 1;

            faces->minColor = 0;
            faces->maxColor = numFaceElements - 1;

            points->minColor = 0;
            points->maxColor = 0;

            numElements = 0;
            numFaceElements = 0;

            for (index_t e = 0; e < totalNumElements; ++e) {
                if (elementTypeId[e] == finalElementTypeId) {
                    elements->Id[numElements] = id[e];
                    elements->Tag[numElements] = tag[e];
                    elements->Color[numElements] = numElements;
                    elements->Owner[numElements] = 0;

                    for (int n = 0; n < elements->m_numVerticesPerElem; ++n) {
                        elements->Nodes[elements->getVertexOffset(numElements, n)] =
                            vertices[INDEX2(n, e, MAX_NUM_NODES_GMSH)];
                    }

                    for (int n = elements->m_numVerticesPerElem;
                             n < elements->m_numTotalNodesPerElem; ++n) {
                        elements->Nodes[elements->getVertexOffset(numElements, n)] = -1;
                    }

                    ++numElements;
                } else if (elementTypeId[e] == finalFaceElementTypeId) {
                    faces->Id[numFaceElements] = id[e];
                    faces->Tag[numFaceElements] = tag[e];
                    faces->Color[numFaceElements] = numFaceElements;
                    faces->Owner[numFaceElements] = 0;

                    for (int n = 0; n < faces->m_numVerticesPerElem; ++n) {
                        faces->Nodes[faces->getVertexOffset(numFaceElements, n)] =
                            vertices[INDEX2(n, e, MAX_NUM_NODES_GMSH)];
                    }

                    for (int n = faces->m_numVerticesPerElem;
                             n < faces->m_numTotalNodesPerElem; ++n) {
                        faces->Nodes[faces->getVertexOffset(numFaceElements, n)] = -1;
                    }

                    ++numFaceElements;
                }
            }
        } else if (line.substr(1, 13) == "PhysicalNames") {
            // Name tags (thanks to Antoine Lefebvre,
            // antoine.lefebvre2@mail.mcgill.ca)
            std::getline(file, line);

            if (file.eof()) {
                delete domain;
                throw IOError(
                    "ShirleyDomain::readGmsh(): early EOF while reading file");
            }

            int numTags = std::stoi(line);

            for (int i = 0; i < numTags; ++i) {
                std::getline(file, line);

                if (file.eof()) {
                    delete domain;
                    throw IOError(
                        "ShirleyDomain::readGmsh(): early EOF while reading file");
                }

                int itmp, tag_key;

                std::stringstream ss(line);
                ss >> itmp >> tag_key;

                std::string name = line.substr(
                    static_cast<unsigned long>(ss.tellg()) + 1);

                // ATTENTION: This is actually the physical dimension, not the
                //  number of entries! See:
                //  http://gmsh.info/doc/texinfo/gmsh.html#MSH-file-format-version-2
                /*
                if (itmp != 2) {
                    delete domain;
                    throw IOError("ShirleyDomain::readGmsh(): "
                                  "expecting two entries per physical name.");
                }
                */

                if (name.length() < 3) {
                    delete domain;
                    throw IOError(
                        "ShirleyDomain::readGmsh(): illegal tagname (\" missing?)");
                }

                name = name.substr(1, name.length() - 2);
                domain->setTagMap(name, tag_key);
            }
        }

        // Search for end of data block
        do {
            std::getline(file, line);

            if (file.eof()) {
                delete domain;

                std::stringstream ss;
                ss << "ShirleyDomain::readGmsh(): unexpected end of file in "
                   << filename;
                throw IOError(ss.str());
            }
        } while (line[0] != '$');
    }

    file.close();

    // Sanity checks
#ifdef READ_GMSH_DEBUG
    std::cout << "Min/max node IDs: "
        << min_nodeId << "/" << max_nodeId << std::endl;
    std::cout << "Min/max vertex IDs: "
        << min_vertexId << "/" << max_vertexId << std::endl;
#endif

    if ((min_nodeId == escript::DataTypes::index_t_max()) ||
        (max_nodeId == escript::DataTypes::index_t_min())) {
        delete domain;
        throw IOError(
            "ShirleyDomain::readGmsh(): no node IDs have been loaded. "
            "Is this a valid mesh file?");
    }

    if (min_vertexId < min_nodeId) {
        delete domain;

        std::stringstream ss;
        ss << "ShirleyDomain::readGmsh(): the minimum vertex ID contained "
           << "in the element descriptors (" << min_vertexId << ") is lower "
           << "than the minimum node ID contained in the node descriptors "
           << "(" << min_nodeId << ")";
        throw IOError(ss.str());
    }

    if (max_vertexId > max_nodeId) {
        delete domain;

        std::stringstream ss;
        ss << "ShirleyDomain::readGmsh(): the maximum vertex ID contained "
           << "in the element descriptors (" << max_vertexId << ") is higher "
           << "than the maximum node ID contained in the node descriptors "
           << "(" << max_nodeId << ")";
        throw IOError(ss.str());
    }

#ifdef READ_GMSH_DEBUG
    std::cout << "Number of elements: "
        << domain->m_elements->numElements << std::endl;
    std::cout << "Number of face elements: "
        << domain->m_faceElements->numElements << std::endl;
    std::cout << "Number of points: "
        << domain->m_points->numElements << std::endl;

    index_t min_elemId = escript::DataTypes::index_t_max();
    index_t max_elemId = escript::DataTypes::index_t_min();

    for (int el = 0; el < domain->m_elements->numElements; ++el) {
        min_elemId = std::min(min_elemId, domain->m_elements->Id[el]);
        max_elemId = std::max(max_elemId, domain->m_elements->Id[el]);
    }

    for (int el = 0; el < domain->m_faceElements->numElements; ++el) {
        min_elemId = std::min(min_elemId, domain->m_faceElements->Id[el]);
        max_elemId = std::max(max_elemId, domain->m_faceElements->Id[el]);
    }

    for (int el = 0; el < domain->m_points->numElements; ++el) {
        min_elemId = std::min(min_elemId, domain->m_points->Id[el]);
        max_elemId = std::max(max_elemId, domain->m_points->Id[el]);
    }

    std::cout << "Min/max element IDs: "
        << min_elemId << "/" << max_elemId << std::endl;

    std::cout << "Number of tags (physical names): "
        << domain->m_tagMap.size() << std::endl;
#endif

    if ((domain->m_elements->numElements < 1) &&
        (domain->m_faceElements->numElements < 1) &&
        (domain->m_points->numElements < 1)) {
        delete domain;
        throw IOError(
            "ShirleyDomain::readGmsh(): no elements have been loaded. "
            "Is this a valid mesh file?");
    }

    // Assign IDs to the SEM nodes.
    tsl::robin_map<index_t, IndexVector> nodeAdjacencyMap;
    tsl::robin_map<IndexPair, index_t,
        boost::hash<std::pair<index_t, index_t>>> edgeInnerNodeMap;
    tsl::robin_map<IndexTriad, index_t, IndexTriadHash> faceInnerNodeMap;
    // At this point <max_nodeId> contains the highest vertex node and its
    // value will not change. We will use <new_nodeId> to keep track of
    // generated SEM nodes.
    index_t new_nodeId = max_nodeId + 1;

    assignIdsToSemNodes(domain->getElements(),
        nodeAdjacencyMap, edgeInnerNodeMap, faceInnerNodeMap, new_nodeId);
    assignIdsToSemNodes(domain->getFaceElements(),
        nodeAdjacencyMap, edgeInnerNodeMap, faceInnerNodeMap, new_nodeId);
    assignIdsToSemNodes(domain->getPoints(),
        nodeAdjacencyMap, edgeInnerNodeMap, faceInnerNodeMap, new_nodeId);

#ifdef READ_GMSH_DEBUG
    std::cout << "Node adjacencies: " << nodeAdjacencyMap.size() << std::endl;
    std::cout << "Unique edges: " << edgeInnerNodeMap.size() << std::endl;
    std::cout << "Unique faces: " << faceInnerNodeMap.size() << std::endl;
    std::cout << "Max node ID: " << (new_nodeId - 1) << std::endl;
    std::cout << "New SEM nodes assigned" << std::endl;
#endif

    domain->m_numUniqueEdges = edgeInnerNodeMap.size();
    domain->m_numUniqueFaces = faceInnerNodeMap.size();

    // Extend the current NodeFile to account for the new SEM nodes
    NodeFile* domNodes = domain->getNodes();
    dim_t orig_numNodes = domNodes->numNodes;

    if (new_nodeId > max_nodeId + 1) {
        index_t numNodesToAdd = new_nodeId - max_nodeId - 1;

#ifdef READ_GMSH_DEBUG
        std::cout << "Nodes to add: " << numNodesToAdd << std::endl;
#endif

        domNodes->addNodeCapacity(numNodesToAdd);
        index_t workNode = max_nodeId + 1;

        for (index_t n = orig_numNodes; n < domNodes->numNodes; ++n) {
            domNodes->Id[n] = workNode;
            domNodes->globalDegreesOfFreedom[n] = domNodes->Id[n];
            domNodes->Tag[n] = 0;
            domNodes->m_nodeType[n] = NodeType_Unknown;

            ++workNode;
        }
    }

    // Maps the ID of each node to its index in the ID array of the node
    // file. We need this mapping in order to make it easier to work with
    // node coordinates later
    tsl::robin_map<index_t, index_t> nodeIdToNodeFileArrayMap;  // Key=node ID, value=array index

    for (index_t n = 0; n < domNodes->numNodes; ++n) {
        index_t node = domNodes->Id[n];
        nodeIdToNodeFileArrayMap[node] = n;
    }

    // Make sure all triangle vertices are in counter-clockwise order.
    // This is necessary because we will need to transform these triangles
    // to a standard triangular element T_st through nodal shape functions
    // later, and these shape functions require triangle vertices to be in
    // counter-clockwise order.
    ensureCounterClockwiseTrianglesIn2D(domain->getElements(),
        domain->getNodes(), numDim, nodeIdToNodeFileArrayMap);
    ensureCounterClockwiseTrianglesIn2D(domain->getFaceElements(),
        domain->getNodes(), numDim, nodeIdToNodeFileArrayMap);
    ensureCounterClockwiseTrianglesIn2D(domain->getPoints(),
        domain->getNodes(), numDim, nodeIdToNodeFileArrayMap);

    // Compute coordinates for the generated SEM nodes
    int xiEtaPolyDegree = order;
    int xiEtaSize = (xiEtaPolyDegree + 1) * (xiEtaPolyDegree + 2) / 2;
    std::vector<double> xi(xiEtaSize);
    std::vector<double> eta(xiEtaSize);
    shirley::LobattoPolynomials::fillXiAndEtaArraysTri3(
        xiEtaPolyDegree, xi.data(), eta.data());  // 2D support only

    std::vector<std::pair<double, double>> xiEtaPairs;
    shirley::LobattoPolynomials::sortXiAndEtaPairs(
        xiEtaSize, xi.data(), eta.data(), xiEtaPairs);

    computeCoordinatesForSemNodes(domain->getElements(),
        domain->getNodes(), numDim, nodeIdToNodeFileArrayMap,
        edgeInnerNodeMap, faceInnerNodeMap, xiEtaPairs);
    computeCoordinatesForSemNodes(domain->getFaceElements(),
        domain->getNodes(), numDim, nodeIdToNodeFileArrayMap,
        edgeInnerNodeMap, faceInnerNodeMap, xiEtaPairs);
    computeCoordinatesForSemNodes(domain->getPoints(),
        domain->getNodes(), numDim, nodeIdToNodeFileArrayMap,
        edgeInnerNodeMap, faceInnerNodeMap, xiEtaPairs);

    edgeInnerNodeMap.clear();
    faceInnerNodeMap.clear();

    // Process traversal strategy
    if (traversalStrategy == CONNECTIVITY_TRAVERSAL) {
        std::cout << "Traversal strategy: connectivity-based" << std::endl;

        double total_reordering_time = 0.0;

        struct timeval timeval_1 = { }, timeval_2 = { };
        double val_tv_1 = 0.0, val_tv_2 = 0.0;
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);
        
        size_t minDegree = std::numeric_limits<size_t>::max();
        index_t minDegreeNode = std::numeric_limits<index_t>::max();
        size_t maxDegree = std::numeric_limits<size_t>::lowest();
        index_t maxDegreeNode = std::numeric_limits<index_t>::lowest();

        for (auto iter_1 = nodeAdjacencyMap.begin();
             iter_1 != nodeAdjacencyMap.end(); ++iter_1) {
            index_t node = iter_1->first;
            // tsl::robin_map uses iter.value() rather than iter->second
            // to provide mutable references
            IndexVector& adjNodes = iter_1.value();
            size_t nodeDegree = adjNodes.size();

            // IMPORTANT: Only vertex nodes will have their degree evaluated
            // here, not SEM nodes!
            if (node <= max_nodeId) {
                if (nodeDegree < minDegree) {
                    minDegree = nodeDegree;
                    minDegreeNode = node;
                }

                if (nodeDegree > maxDegree) {
                    maxDegree = nodeDegree;
                    maxDegreeNode = node;
                }
            }
        }

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);
        
#ifdef READ_GMSH_DEBUG
        std::cout << "Min. degree: " << minDegree << std::endl;
        std::cout << "Min. deg. node: " << minDegreeNode << std::endl;
        std::cout << "Max. degree: " << maxDegree << std::endl;
        std::cout << "Max. deg. node: " << maxDegreeNode << std::endl;
#endif

        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);
        
        // Utility class to allow sorting nodes from lowest to highest degree
        struct {
            bool operator()(
                const std::pair<index_t, size_t>& nodeAndDegree1,
                const std::pair<index_t, size_t>& nodeAndDegree2) const
            {
                // The "lower" item will be the node with the lower degree
                if (nodeAndDegree1.second < nodeAndDegree2.second) {
                    return true;
                }

                if (nodeAndDegree1.second > nodeAndDegree2.second) {
                    return false;
                }

                // Node ID
                return nodeAndDegree1.first < nodeAndDegree2.first;
            }
        } nodeAndDegreePairLess;

        // Builds a map where each node ID is bound to a vector of
        // elements that reference that node
        tsl::robin_map<int, std::vector<int>> nodeIdToElemIndexMap;
        fillNodeIdToElemIndexMap(domain->getElements(), domain->getNodes(),
            numDim, nodeIdToElemIndexMap);

        {
        // Node relabeling via the Cuthill-McKee algorithm (see
        // https://en.wikipedia.org/wiki/Cuthill-McKee_algorithm and
        // http://ciprian-zavoianu.blogspot.com/2009/01/project-bandwidth-reduction.html).
        // After this procedure node IDs within elements should be closer to
        // each other, which should result in a lower number of cache misses
        // when traversing elements and their nodes later.
        // We start by storing a peripheral node in the R set.
        // We also need a separate map that allows us to quickly check whether
        // a certain node is already stored in the R set (see <nodes_in_r>).
        // We use boost::dynamic_bitset<> for this instead of
        // {std::unordered_map|tsl:robin_map}<bool> because of its optimized
        // storage features.
        tsl::robin_map<index_t, index_t> r_set;  // Key=new node, value=original node
        index_t r_add = 1, r_look = 1;
        r_set[r_add] = minDegreeNode;

        int processedNodes = 0;

        index_t currNode = minDegreeNode;

        boost::dynamic_bitset<> nodes_in_r(
            new_nodeId - min_nodeId, 0);
        boost::dynamic_bitset<> elem_indices_in_r(
            domain->getElements()->numElements, 0);
        std::vector<int> elemIdxSequence;
        std::vector<std::pair<index_t, size_t>> nodesAndDegrees;

        auto nodeAdjMapEnd = nodeAdjacencyMap.end();

        while (processedNodes < domNodes->numNodes) {
            ++processedNodes;

            // Mark <currNode> as already stored in the R set
            nodes_in_r[currNode - min_nodeId] = true;

            // Read the element indices that reference <currNode>
            // and store them in the traversal sequence
            std::vector<int>& elemIndices = nodeIdToElemIndexMap[currNode];

            for (int elemIdx : elemIndices) {
                if (!elem_indices_in_r[elemIdx]) {
                    elemIdxSequence.push_back(elemIdx);
                    elem_indices_in_r[elemIdx] = true;
                }
            }

            // Collect the nodes adjacent to <currNode> for processing.
            // Nodes already contained in the R set are excluded.
            std::vector<index_t>& adjNodes_1 = nodeAdjacencyMap[currNode];
            nodesAndDegrees.clear();

            for (int adjNode : adjNodes_1) {
                if (!nodes_in_r[adjNode - min_nodeId]) {
                    size_t degree = 0;
                    auto iter = nodeAdjacencyMap.find(adjNode);

                    if (iter != nodeAdjMapEnd) {
                        // tsl::robin_map uses iter.value() rather than iter->second
                        // to provide mutable references
                        IndexVector& adjNodes_2 = iter.value();
                        degree = adjNodes_2.size();
                    }

                    nodesAndDegrees.emplace_back(adjNode, degree);
                }
            }

            if (!nodesAndDegrees.empty()) {
                // Sort the A set with ascending degree
                std::sort(nodesAndDegrees.begin(), nodesAndDegrees.end(),
                    nodeAndDegreePairLess);

                // Mark the adjacent nodes as already stored in the R set
                for (auto nodeAndDegree : nodesAndDegrees) {
                    ++r_add;
                    r_set[r_add] = nodeAndDegree.first;
                    nodes_in_r[nodeAndDegree.first - min_nodeId] = true;
                }

                // If all nodes have already been stored in R, we're done
                if (r_set.size() >= static_cast<std::size_t>(domNodes->numNodes)) {
                    break;
                }
            }

            // Pick a new node to process
            ++r_look;

            if (r_look < new_nodeId) {
                currNode = r_set[r_look];
            }
            else {
                break;
            }
        }

        nodeIdToElemIndexMap.clear();

#ifdef READ_GMSH_DEBUG
        std::cout << "Items in element traversal sequence: "
                  << elemIdxSequence.size() << std::endl;
        std::cout << "Element traversal sequence: " << std::endl;

        for (int i = 0;
             i < std::min(static_cast<int>(elemIdxSequence.size()), 10); ++i) {
            if (i > 0) { std::cout << ","; }
            std::cout << elemIdxSequence[i];
        }

        std::cout << std::endl;
#endif

        // Sanity check
        for (index_t i = min_nodeId; i < new_nodeId; ++i) {
            if (!nodes_in_r[i - min_nodeId]) {
                delete domain;

                std::stringstream ss;
                ss << "ShirleyDomain::readGmsh(): "
                      "connectivity-based traversal: "
                      "node " << i << " has not been visited";
                throw IOError(ss.str());
            }
        }

        nodes_in_r.resize(0, false);
        nodes_in_r.shrink_to_fit();

        elem_indices_in_r.resize(0, false);
        elem_indices_in_r.shrink_to_fit();

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);
        
#ifdef READ_GMSH_DEBUG
        std::cout << "Passed sanity check" << std::endl;
        std::cout << "r_look: " << r_look
            << ", r_add: " << r_add
            << ", r_set.size(): " << r_set.size()
            << std::endl;
#endif

#define MUST_REPOSITION_ELEMENTS

#ifdef MUST_REPOSITION_ELEMENTS
        {
        // Repositions the elements in memory so that they are traversed
        // more efficiently later
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);
            
        ElementFile *oldElements = domain->getElements();
        dim_t numElements = oldElements->numElements;
        int numTotalNodesPerElem = oldElements->m_numTotalNodesPerElem;

        ElementFile *newElements = new ElementFile(
            oldElements->elemTypeId, mpiInfo, order);
        newElements->allocTable(numElements);

        newElements->minColor = 0;
        newElements->maxColor = numElements - 1;
        newElements->tagsInUse.insert(newElements->tagsInUse.end(),
            oldElements->tagsInUse.begin(), oldElements->tagsInUse.end());
        newElements->elemTypeName = oldElements->elemTypeName;

//#pragma omp parallel for
        for (index_t el = 0; el < numElements; ++el) {
            int oldElementIdx = elemIdxSequence[el];

            newElements->Id[el] = oldElements->Id[oldElementIdx];
            newElements->Tag[el] = oldElements->Tag[oldElementIdx];
            newElements->Owner[el] = oldElements->Owner[oldElementIdx];
            newElements->Color[el] = oldElements->Color[oldElementIdx];

            for (int n = 0; n < numTotalNodesPerElem; n++) {
                newElements->Nodes[INDEX2(n, el, numTotalNodesPerElem)] =
                    oldElements->Nodes[INDEX2(n, oldElementIdx, numTotalNodesPerElem)];
            }
        }

        // <oldElements> goes stale here, as setElements() calls the
        // delete operator on it
        domain->setElements(newElements);
        
        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);
        }
#endif  // MUST_REPOSITION_ELEMENTS

#define MUST_RELABEL_NODES

#ifdef MUST_RELABEL_NODES
        {
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);
            
        tsl::robin_map<index_t, index_t> new_r_set;  // Key=original node, value=new node

        for (auto iter = r_set.begin(); iter != r_set.end(); ) {
            new_r_set[iter->second] = iter->first;
            iter = r_set.erase(iter);
        }

        // Relabel node IDs using the results of the Cuthill-McKee algorithm.
        // NodeFile
        index_t origNode, newNode;

        for (index_t n = 0; n < domNodes->numNodes; ++n) {
            origNode = domNodes->Id[n];
            newNode = new_r_set[origNode];
            domNodes->Id[n] = newNode;
            domNodes->globalDegreesOfFreedom[n] = domNodes->Id[n];
        }

        // Elements
        ElementFile *elements = domain->getElements();
        index_t *elemNodes = elements->Nodes;
        int nodeOffset;

//#pragma omp parallel for private(nodeOffset, origNode, newNode)
        for (int el = 0; el < elements->numElements; ++el) {
            for (int n = 0; n < elements->m_numTotalNodesPerElem; ++n) {
                nodeOffset = elements->getVertexOffset(el, n);
                origNode = elemNodes[nodeOffset];
                newNode = new_r_set[origNode];
                elemNodes[nodeOffset] = newNode;
            }
        }

        // Face elements
        elements = domain->getFaceElements();
        elemNodes = elements->Nodes;

//#pragma omp parallel for private(nodeOffset, origNode, newNode)
        for (int el = 0; el < elements->numElements; ++el) {
            for (int n = 0; n < elements->m_numTotalNodesPerElem; ++n) {
                nodeOffset = elements->getVertexOffset(el, n);
                origNode = elemNodes[nodeOffset];
                newNode = new_r_set[origNode];
                elemNodes[nodeOffset] = newNode;
            }
        }

        // Points
        elements = domain->getPoints();
        elemNodes = elements->Nodes;

//#pragma omp parallel for private(nodeOffset, origNode, newNode)
        for (int el = 0; el < elements->numElements; ++el) {
            for (int n = 0; n < elements->m_numTotalNodesPerElem; ++n) {
                nodeOffset = elements->getVertexOffset(el, n);
                origNode = elemNodes[nodeOffset];
                newNode = new_r_set[origNode];
                elemNodes[nodeOffset] = newNode;
            }
        }

        new_r_set.clear();
        
        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);
        }
#endif  // MUST_RELABEL_NODES
        }

        std::cout << "Connectivity: Total memory reordering time = "
                  << total_reordering_time << " s" << std::endl;
    }
    else if (traversalStrategy == DISTANCE_TRAVERSAL) {
        std::cout << "Traversal strategy: distance-based" << std::endl;

        // Computes the minimum and maximum coordinates of the domain,
        // and also the related nodes
        double total_reordering_time = 0.0;

        struct timeval timeval_1 = { }, timeval_2 = { };
        double val_tv_1 = 0.0, val_tv_2 = 0.0;
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);
        
        std::tuple<double, double, double, double, index_t, index_t>
            minMaxCoordsAndNodes = getMinMaxCoordsAndNodes(
                domain->getElements(), domain->getNodes(),
                numDim, nodeIdToNodeFileArrayMap);

        double minX = std::get<0>(minMaxCoordsAndNodes);
        double minY = std::get<1>(minMaxCoordsAndNodes);
        double maxX = std::get<2>(minMaxCoordsAndNodes);
        double maxY = std::get<3>(minMaxCoordsAndNodes);
        index_t min_coords_node = std::get<4>(minMaxCoordsAndNodes);
        index_t max_coords_node = std::get<5>(minMaxCoordsAndNodes);

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);
        
#ifdef READ_GMSH_DEBUG
        std::cout << "MinX = " << minX << std::endl;
        std::cout << "MinY = " << minY << std::endl;
        std::cout << "MaxX = " << maxX << std::endl;
        std::cout << "MaxY = " << maxY << std::endl;
        std::cout << "Min. coords. node = " << min_coords_node << std::endl;
        std::cout << "Max. coords. node = " << max_coords_node << std::endl;
#endif

        // Computes distance from minimum coordinates of the domain
        // to each node (SEM nodes included), and stores the result
        // in a map
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);
        
        tsl::robin_map<int, std::tuple<double, double, double>>
            nodeIdToDistToRefMap;
        computeDistToRefAtNodes(domain->getElements(), domain->getNodes(),
            numDim, minX, minY, nodeIdToNodeFileArrayMap, nodeIdToDistToRefMap);

        // Utility class to allow sorting nodes from lowest to highest
        // distance to a reference point
        struct {
            bool operator()(
                const std::pair<index_t, double>& nodeAndDist1,
                const std::pair<index_t, double>& nodeAndDist2) const
            {
                // The "lower" node will be the one with the lower distance
                if (nodeAndDist1.second < nodeAndDist2.second) {
                    return true;
                }

                if (nodeAndDist1.second > nodeAndDist2.second) {
                    return false;
                }

                // Node ID
                return nodeAndDist1.first < nodeAndDist2.first;
            }
        } nodeAndDistPairLess;

        // Builds a map where each node ID is bound to a vector of
        // elements that reference that node
        tsl::robin_map<int, std::vector<int>> nodeIdToElemIndexMap;
        fillNodeIdToElemIndexMap(domain->getElements(), domain->getNodes(),
            numDim, nodeIdToElemIndexMap);

        {
        // The section below borrows heavily from the Cuthill-McKee
        // algorithm.
        // We add nodes to the R set as we traverse the domain going
        // from the nodes closest to the minimum coordinates to those
        // farthest from them.
        tsl::robin_map<index_t, index_t> r_set;  // Key=new node, value=original node
        index_t r_add = 1, r_look = 1;
        r_set[r_add] = min_coords_node;

        int processedNodes = 0;

        index_t currNode = min_coords_node;

        boost::dynamic_bitset<> nodes_in_r(
            new_nodeId - min_nodeId, 0);
        boost::dynamic_bitset<> elem_indices_in_r(
            domain->getElements()->numElements, 0);
        std::vector<std::pair<index_t, double>> nodesAndDistances;
        std::vector<int> elemIdxSequence;

        while (processedNodes < domNodes->numNodes) {
            ++processedNodes;

            // Mark <currNode> as already stored in the R set
            nodes_in_r[currNode - min_nodeId] = true;

            // Read the element indices that reference <currNode>
            // and store them in the traversal sequence
            std::vector<int>& elemIndices = nodeIdToElemIndexMap[currNode];

            for (int elemIdx : elemIndices) {
                if (!elem_indices_in_r[elemIdx]) {
                    elemIdxSequence.push_back(elemIdx);
                    elem_indices_in_r[elemIdx] = true;
                }
            }

            // Collect the nodes adjacent to <currNode> for processing.
            // Nodes already contained in the R set are excluded.
            std::vector<index_t>& adjNodes = nodeAdjacencyMap[currNode];
            nodesAndDistances.clear();

            for (int adjNode : adjNodes) {
                if (!nodes_in_r[adjNode - min_nodeId]) {
                    double distance = std::get<0>(nodeIdToDistToRefMap[adjNode]);
                    nodesAndDistances.emplace_back(adjNode, distance);
                }
            }

            if (!nodesAndDistances.empty()) {
                // Sort the adjacent node vector with ascending distance
                std::sort(nodesAndDistances.begin(), nodesAndDistances.end(),
                    nodeAndDistPairLess);

                // Mark the adjacent nodes as already stored in the R set
                for (auto& nodeAndDistance : nodesAndDistances) {
                    ++r_add;
                    r_set[r_add] = nodeAndDistance.first;
                    nodes_in_r[nodeAndDistance.first - min_nodeId] = true;
                }

                // If all nodes have already been stored in R, we're done
                if (r_set.size() >= static_cast<std::size_t>(domNodes->numNodes)) {
                    break;
                }
            }

            // Pick a new node to process
            ++r_look;

            if (r_look < new_nodeId) {
                currNode = r_set[r_look];
            }
            else {
                break;
            }
        }

        nodeIdToElemIndexMap.clear();
        nodeIdToDistToRefMap.clear();

#ifdef READ_GMSH_DEBUG
        std::cout << "Items in element traversal sequence: "
                  << elemIdxSequence.size() << std::endl;
        std::cout << "Element traversal sequence: " << std::endl;

        for (int i = 0;
             i < std::min(static_cast<int>(elemIdxSequence.size()), 10); ++i) {
            if (i > 0) { std::cout << ","; }
            std::cout << elemIdxSequence[i];
        }

        std::cout << std::endl;
#endif

        // Sanity check
        for (index_t i = min_nodeId; i < new_nodeId; ++i) {
            if (!nodes_in_r[i - min_nodeId]) {
                delete domain;

                std::stringstream ss;
                ss << "ShirleyDomain::readGmsh(): "
                      "distance-based traversal: "
                      "node " << i << " has not been visited";
                throw IOError(ss.str());
            }
        }

        nodes_in_r.resize(0, false);
        nodes_in_r.shrink_to_fit();

        elem_indices_in_r.resize(0, false);
        elem_indices_in_r.shrink_to_fit();

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);
        
#ifdef READ_GMSH_DEBUG
        std::cout << "Passed sanity check" << std::endl;
        std::cout << "r_look: " << r_look
            << ", r_add: " << r_add
            << ", r_set.size(): " << r_set.size()
            << std::endl;
#endif

#define MUST_REPOSITION_ELEMENTS

#ifdef MUST_REPOSITION_ELEMENTS
        {
        // Repositions the elements in memory so that they are traversed
        // more efficiently later.
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);
            
        ElementFile *oldElements = domain->getElements();
        dim_t numElements = oldElements->numElements;
        int numTotalNodesPerElem = oldElements->m_numTotalNodesPerElem;

        ElementFile *newElements = new ElementFile(
            oldElements->elemTypeId, mpiInfo, order);
        newElements->allocTable(numElements);

        newElements->minColor = 0;
        newElements->maxColor = numElements - 1;
        newElements->tagsInUse.insert(newElements->tagsInUse.end(),
            oldElements->tagsInUse.begin(), oldElements->tagsInUse.end());
        newElements->elemTypeName = oldElements->elemTypeName;

//#pragma omp parallel for
        for (index_t el = 0; el < numElements; ++el) {
            int oldElementIdx = elemIdxSequence[el];

            newElements->Id[el] = oldElements->Id[oldElementIdx];
            newElements->Tag[el] = oldElements->Tag[oldElementIdx];
            newElements->Owner[el] = oldElements->Owner[oldElementIdx];
            newElements->Color[el] = oldElements->Color[oldElementIdx];

            for (int n = 0; n < numTotalNodesPerElem; n++) {
                newElements->Nodes[INDEX2(n, el, numTotalNodesPerElem)] =
                    oldElements->Nodes[INDEX2(n, oldElementIdx, numTotalNodesPerElem)];
            }
        }

        // <oldElements> goes stale here, as setElements() calls the
        // delete operator on it
        domain->setElements(newElements);
        
        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);
        }
#endif

#define MUST_RELABEL_NODES

#ifdef MUST_RELABEL_NODES
        {
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);
            
        tsl::robin_map<index_t, index_t> new_r_set;  // Key=original node, value=new node

        for (auto iter = r_set.begin(); iter != r_set.end(); ) {
            new_r_set[iter->second] = iter->first;
            iter = r_set.erase(iter);
        }

        // Relabel node IDs using the results of the modified
        // Cuthill-McKee algorithm.
        // NodeFile
        index_t origNode, newNode;

        for (index_t n = 0; n < domNodes->numNodes; ++n) {
            origNode = domNodes->Id[n];
            newNode = new_r_set[origNode];
            domNodes->Id[n] = newNode;
            domNodes->globalDegreesOfFreedom[n] = domNodes->Id[n];
        }

        // Elements
        ElementFile *elements = domain->getElements();
        index_t *elemNodes = elements->Nodes;
        int nodeOffset;

//#pragma omp parallel for private(nodeOffset, origNode, newNode)
        for (int el = 0; el < elements->numElements; ++el) {
            for (int n = 0; n < elements->m_numTotalNodesPerElem; ++n) {
                nodeOffset = elements->getVertexOffset(el, n);
                origNode = elemNodes[nodeOffset];
                newNode = new_r_set[origNode];
                elemNodes[nodeOffset] = newNode;
            }
        }

        // Face elements
        elements = domain->getFaceElements();
        elemNodes = elements->Nodes;

//#pragma omp parallel for private(nodeOffset, origNode, newNode)
        for (int el = 0; el < elements->numElements; ++el) {
            for (int n = 0; n < elements->m_numTotalNodesPerElem; ++n) {
                nodeOffset = elements->getVertexOffset(el, n);
                origNode = elemNodes[nodeOffset];
                newNode = new_r_set[origNode];
                elemNodes[nodeOffset] = newNode;
            }
        }

        // Points
        elements = domain->getPoints();
        elemNodes = elements->Nodes;

//#pragma omp parallel for private(nodeOffset, origNode, newNode)
        for (int el = 0; el < elements->numElements; ++el) {
            for (int n = 0; n < elements->m_numTotalNodesPerElem; ++n) {
                nodeOffset = elements->getVertexOffset(el, n);
                origNode = elemNodes[nodeOffset];
                newNode = new_r_set[origNode];
                elemNodes[nodeOffset] = newNode;
            }
        }

        new_r_set.clear();
        }
#endif  // MUST_RELABEL_NODES
        }
        
        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);

        std::cout << "Distance: Total memory reordering time = "
                  << total_reordering_time << " s" << std::endl;
    }
    else if (traversalStrategy == SPACE_FILLING_CURVE_TRAVERSAL) {
        std::cout << "Traversal strategy: space-filling curve" << std::endl;

        // Computes the minimum and maximum coordinates of the domain,
        // and also the related nodes
        double total_reordering_time = 0.0;

        struct timeval timeval_1 = { }, timeval_2 = { };
        double val_tv_1 = 0.0, val_tv_2 = 0.0;
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);

        std::tuple<double, double, double, double, index_t, index_t>
            minMaxCoordsAndNodes = getMinMaxCoordsAndNodes(
                domain->getElements(), domain->getNodes(),
                numDim, nodeIdToNodeFileArrayMap);

        double minX = std::get<0>(minMaxCoordsAndNodes);
        double minY = std::get<1>(minMaxCoordsAndNodes);
        double maxX = std::get<2>(minMaxCoordsAndNodes);
        double maxY = std::get<3>(minMaxCoordsAndNodes);
        index_t min_coords_node = std::get<4>(minMaxCoordsAndNodes);
        index_t max_coords_node = std::get<5>(minMaxCoordsAndNodes);

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);
        
#ifdef READ_GMSH_DEBUG
        std::cout << "MinX = " << minX << std::endl;
        std::cout << "MinY = " << minY << std::endl;
        std::cout << "MaxX = " << maxX << std::endl;
        std::cout << "MaxY = " << maxY << std::endl;
        std::cout << "Min. coords. node = " << min_coords_node << std::endl;
        std::cout << "Max. coords. node = " << max_coords_node << std::endl;
        std::cout << "SFC: Min/max coords. time = "
                  << (val_tv_2 - val_tv_1) << " s" << std::endl;
#endif

        // Generates a Hilbert space-filling curve to try and traverse
        // the elements and nodes in an optimized manner
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);

        double xRange = std::abs(maxX - minX);
        double yRange = std::abs(maxY - minY);
        double xyRatio = xRange / yRange;
        int numElemsDimYGuess = std::max({
            std::ceil(std::sqrt(domain->getElements()->numElements) / xyRatio),
            3.0 });
        int numElemsDimXGuess = std::max({
            std::ceil(std::sqrt(domain->getElements()->numElements)),
            3.0 });

        int curveWidth = numElemsDimXGuess;
        int curveHeight = numElemsDimYGuess;

        if (curveWidth % 2 != 0) {
            ++curveWidth;
            numElemsDimXGuess = curveWidth;
        }

        if (curveHeight % 2 != 0) {
            ++curveHeight;
            numElemsDimYGuess = curveHeight;
        }

        std::vector<std::pair<double, double>> curveVec;
        int maxDepth = 1;

        if (curveWidth >= curveHeight) {
            gilbert2d_vector(0, 0, curveWidth, 0,
                0, curveHeight, curveVec, maxDepth);
        }
        else {
            gilbert2d_vector(0, 0, 0, curveHeight,
                curveWidth, 0, curveVec, maxDepth);
        }

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);

        total_reordering_time += (val_tv_2 - val_tv_1);
        
#ifdef READ_GMSH_DEBUG
        std::cout << "Space-filling curve:" << curveVec.size()
                  << " elements" << std::endl;
        std::cout << "Space-filling curve: max. depth = "
                  << maxDepth << std::endl;
        std::cout << "SFC: Curve generation time = "
                  << (val_tv_2 - val_tv_1) << " s" << std::endl;
#endif

        // Computes centroid coordinates for all elements
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);

        std::vector<std::tuple<int, double, double>> centroidVec;
        computeCentroidVector(domain->getElements(), domain->getNodes(),
            numDim, nodeIdToNodeFileArrayMap, centroidVec);

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);
        
        total_reordering_time += (val_tv_2 - val_tv_1);

#ifdef READ_GMSH_DEBUG
        std::cout << "SFC: Centroid computation time = "
                  << (val_tv_2 - val_tv_1) << " s" << std::endl;
#endif

        // Determines the subdivision of the mesh according to the
        // rectangle of the space-filling curve
        double xStep = curveWidth / xRange;
        double yStep = curveHeight / yRange;

#ifdef READ_GMSH_DEBUG
        std::cout << "Elements in X-axis (guess) = "
                  << numElemsDimXGuess << std::endl;
        std::cout << "Elements in Y-axis (guess) = "
                  << numElemsDimYGuess << std::endl;
        std::cout << "xStep = " << xStep << std::endl;
        std::cout << "yStep = " << yStep << std::endl;
#endif

        // For each x/y slot dictated by the subdivision of the mesh,
        // store the elements of the mesh matching that region
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);

        std::vector<std::vector<int>> elemIdxCurveMatrix(curveVec.size());

        for (auto &centTuple : centroidVec) {
            int elemIdx = std::get<0>(centTuple);
            double centX = std::get<1>(centTuple);
            double centY = std::get<2>(centTuple);

            int xSlot = static_cast<int>(std::round((centX - minX) * xStep));
            int ySlot = static_cast<int>(std::round((centY - minY) * yStep));

            if (xSlot >= curveWidth) {
                xSlot = curveWidth - 1;
            }

            if (ySlot >= curveHeight) {
                ySlot = curveHeight - 1;
            }

            std::vector<std::vector<int>>::size_type slot =
                static_cast<std::vector<std::vector<int>>::size_type>(
                    ySlot * curveWidth) + xSlot;

            if (slot < elemIdxCurveMatrix.size()) {
                elemIdxCurveMatrix[slot].push_back(elemIdx);
            }
        }

        centroidVec.clear();

        // Traverse the space-filling curve, locating at each step the
        // relevant x/y slot dictated by the subdivision of the mesh;
        // we'll store the elements bound to each slot as we move along
        std::vector<int> elemIdxSequence;
        boost::dynamic_bitset<> elem_indices(
            domain->getElements()->numElements, 0);

        for (auto &curveXy : curveVec) {
            double curveX = curveXy.first;
            double curveY = curveXy.second;

            int xSlot = static_cast<int>(curveX);
            int ySlot = static_cast<int>(curveY);
            std::vector<std::vector<int>>::size_type slot =
                static_cast<std::vector<std::vector<int>>::size_type>(
                    ySlot * curveWidth) + xSlot;

            if (slot < elemIdxCurveMatrix.size()) {
                std::vector<int> &elemIdsAtSlot = elemIdxCurveMatrix[slot];

                for (auto elemIdx : elemIdsAtSlot) {
                    if (!elem_indices[elemIdx]) {
                        elemIdxSequence.push_back(elemIdx);
                        elem_indices[elemIdx] = true;
                    }
                }
            }
        }

        curveVec.clear();
        elemIdxCurveMatrix.clear();

        // Process any "leftover" elements
        for (int i = 0; i < domain->getElements()->numElements; ++i) {
            // This should never happen, but we're not taking any chances
            if (!elem_indices[i]) {
                elemIdxSequence.push_back(i);
                elem_indices[i] = true;
            }
        }

        elem_indices.resize(0, false);
        elem_indices.shrink_to_fit();

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);
        
        total_reordering_time += (val_tv_2 - val_tv_1);

#ifdef READ_GMSH_DEBUG
        std::cout << "Elements in space-filling curve: "
                  << elemIdxSequence.size() << std::endl;
        std::cout << "SFC: Element distribution time = "
                  << (val_tv_2 - val_tv_1) << " s" << std::endl;
        std::cout << "Element IDs in space-filling curve:" << std::endl;

        for (int i = 0;
             i < std::min(static_cast<int>(elemIdxSequence.size()), 10); ++i) {
            if (i > 0) { std::cout << ','; }
            std::cout << elemIdxSequence[i];
        }

        std::cout << std::endl;
#endif

#define MUST_REPOSITION_ELEMENTS

#ifdef MUST_REPOSITION_ELEMENTS
        {
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);

        // Repositions the elements in memory so that they are traversed
        // more efficiently later.
        ElementFile *oldElements = domain->getElements();
        dim_t numElements = oldElements->numElements;
        int numTotalNodesPerElem = oldElements->m_numTotalNodesPerElem;

        ElementFile *newElements = new ElementFile(
            oldElements->elemTypeId, mpiInfo, order);
        newElements->allocTable(numElements);

        newElements->minColor = 0;
        newElements->maxColor = numElements - 1;
        newElements->tagsInUse.insert(newElements->tagsInUse.end(),
            oldElements->tagsInUse.begin(), oldElements->tagsInUse.end());
        newElements->elemTypeName = oldElements->elemTypeName;

//#pragma omp parallel for
        for (index_t el = 0; el < numElements; ++el) {
            int oldElementIdx = elemIdxSequence[el];

            newElements->Id[el] = oldElements->Id[oldElementIdx];
            newElements->Tag[el] = oldElements->Tag[oldElementIdx];
            newElements->Owner[el] = oldElements->Owner[oldElementIdx];
            newElements->Color[el] = oldElements->Color[oldElementIdx];

            for (int n = 0; n < numTotalNodesPerElem; n++) {
                newElements->Nodes[INDEX2(n, el, numTotalNodesPerElem)] =
                    oldElements->Nodes[INDEX2(n, oldElementIdx, numTotalNodesPerElem)];
            }
        }

        domain->setElements(newElements);  // <oldElements> goes stale here

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);
        
        total_reordering_time += (val_tv_2 - val_tv_1);

        std::cout << "SFC: Element relabelling & reordering time = "
                  << (val_tv_2 - val_tv_1) << " s" << std::endl;
        }
#endif

#define MUST_RELABEL_NODES

#ifdef MUST_RELABEL_NODES
        {
        gettimeofday(&timeval_1, nullptr);
        val_tv_1 = timeval_1.tv_sec + (timeval_1.tv_usec * 0.000001);

        // Utility class to allow sorting nodes from lowest to highest degree
        struct {
            bool operator()(
                const std::pair<index_t, size_t>& nodeAndDegree1,
                const std::pair<index_t, size_t>& nodeAndDegree2) const
            {
                // The "lower" item will be the node with the lower degree
                if (nodeAndDegree1.second < nodeAndDegree2.second) {
                    return true;
                }

                if (nodeAndDegree1.second > nodeAndDegree2.second) {
                    return false;
                }

                // Node ID
                return nodeAndDegree1.first < nodeAndDegree2.first;
            }
        } nodeAndDegreePairLess;

        // At this point, the elements of the domain have already
        // been repositioned in an optimized sequential manner as
        // dictated by the space-filling curve. So now, all that
        // needs to be done is to traverse the elements sequentially
        // and collect their SEM nodes as they appear in order to
        // relabel the nodes later.
        ElementFile *elements = domain->getElements();
        dim_t numElements = elements->numElements;
        index_t *elemNodes = elements->Nodes;
        int numTotalNodesPerElem = elements->m_numTotalNodesPerElem;

        tsl::robin_map<index_t, index_t> new_r_set;  // Key=original node, value=new node
        index_t new_r_add = 1;

        std::vector<std::pair<index_t, size_t>> nodesAndDegrees;
        auto nodeAdjMapEnd = nodeAdjacencyMap.end();

        for (int el = 0; el < numElements; ++el) {
            // Collects the SEM nodes of this element that haven't
            // been processed yet, along with their degrees (i.e.
            // how many other SEM nodes they are connected with)
            nodesAndDegrees.clear();
            auto new_r_set_end = new_r_set.end();

            for (int p = 0; p < numTotalNodesPerElem; ++p) {
                index_t nodeId = elemNodes[elements->getVertexOffset(el, p)];

                if (new_r_set.find(nodeId) == new_r_set_end) {
                    size_t nodeDegree = 0;
                    auto iter = nodeAdjacencyMap.find(nodeId);

                    if (iter != nodeAdjMapEnd) {
                        // tsl::robin_map uses iter.value() rather than iter->second
                        // to provide mutable references
                        IndexVector& adjNodes = iter.value();
                        nodeDegree = adjNodes.size();
                    }

                    nodesAndDegrees.emplace_back(nodeId, nodeDegree);
                }
            }

            // Sorts the node/degree vector of this element
            std::sort(nodesAndDegrees.begin(), nodesAndDegrees.end(),
                nodeAndDegreePairLess);

            // Determines the new IDs for the nodes of this element
            for (std::pair<index_t, size_t> &nodeAndDegree : nodesAndDegrees) {
                index_t nodeId = nodeAndDegree.first;
                new_r_set[nodeId] = new_r_add;
                ++new_r_add;
            }
        }

#ifdef READ_GMSH_DEBUG
        std::cout << "new_r_add: " << new_r_add
            << ", new_r_set.size(): " << new_r_set.size()
            << std::endl;
#endif

        // Relabel node IDs using the results of the space-filling traversal.
        // NodeFile
        index_t origNode, newNode;

        for (index_t n = 0; n < domNodes->numNodes; ++n) {
            origNode = domNodes->Id[n];
            newNode = new_r_set[origNode];
            domNodes->Id[n] = newNode;
            domNodes->globalDegreesOfFreedom[n] = domNodes->Id[n];
        }

        // Elements
        int nodeOffset;

//#pragma omp parallel for private(nodeOffset, origNode, newNode)
        for (int el = 0; el < elements->numElements; ++el) {
            for (int n = 0; n < elements->m_numTotalNodesPerElem; ++n) {
                nodeOffset = elements->getVertexOffset(el, n);
                origNode = elemNodes[nodeOffset];
                newNode = new_r_set[origNode];
                elemNodes[nodeOffset] = newNode;
            }
        }

        // Face elements
        elements = domain->getFaceElements();
        elemNodes = elements->Nodes;

//#pragma omp parallel for private(nodeOffset, origNode, newNode)
        for (int el = 0; el < elements->numElements; ++el) {
            for (int n = 0; n < elements->m_numTotalNodesPerElem; ++n) {
                nodeOffset = elements->getVertexOffset(el, n);
                origNode = elemNodes[nodeOffset];
                newNode = new_r_set[origNode];
                elemNodes[nodeOffset] = newNode;
            }
        }

        // Points
        elements = domain->getPoints();
        elemNodes = elements->Nodes;

//#pragma omp parallel for private(nodeOffset, origNode, newNode)
        for (int el = 0; el < elements->numElements; ++el) {
            for (int n = 0; n < elements->m_numTotalNodesPerElem; ++n) {
                nodeOffset = elements->getVertexOffset(el, n);
                origNode = elemNodes[nodeOffset];
                newNode = new_r_set[origNode];
                elemNodes[nodeOffset] = newNode;
            }
        }

        new_r_set.clear();

        gettimeofday(&timeval_2, nullptr);
        val_tv_2 = timeval_2.tv_sec + (timeval_2.tv_usec * 0.000001);
        
        total_reordering_time += (val_tv_2 - val_tv_1);

        std::cout << "SFC: Node relabelling & reordering time = "
                  << (val_tv_2 - val_tv_1) << " s" << std::endl;
        }
#endif  // MUST_RELABEL_NODES

        std::cout << "SFC: Total memory reordering time = "
                  << total_reordering_time << " s" << std::endl;
    }
    else {
        std::cout << "Traversal strategy: NONE" << std::endl;
    }

    nodeAdjacencyMap.clear();
    nodeIdToNodeFileArrayMap.clear();

    // Additional procedures.
    // ATTENTION: after the call to domain->resolveNodeIds(), the
    // semantics of the domain->{m_elements|m_faceElements|points}->Nodes
    // array will change! The Nodes array will no longer contain node IDs,
    // but indexes to domain->m_nodes->{Id|Coordinates}.
    domain->resolveNodeIds();
    domain->prepare(optimize);

    domain->fillJacobians();
    domain->fillCanonicalNodeDerivatives();
    domain->fillXiEtaCoordinates();
    domain->fillXiEtaTraversal();

#ifdef READ_GMSH_DEBUG
        std::cout << "Number of colors: "
            << (domain->getElements()->maxColor -
                    domain->getElements()->minColor + 1)
            << std::endl;
#endif

    return domain->getPtr();
}

}  // namespace shirley
