#!/bin/bash

##############################################################################
#
# esys-escript:
# Copyright (c) 2003-2021 by The University of Queensland
# http://www.uq.edu.au
#
# Primary Business: Queensland, Australia
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
# Development until 2012 by Earth Systems Science Computational Center (ESSCC)
# Development 2012-2013 by School of Earth Sciences
# Development from 2014 by Centre for Geoscience Computing (GeoComp)
# Development from 2019 by School of Earth and Environmental Sciences
#
# Shirley domain module:
# Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
# and The University of Queensland
# https://www.ufrn.br
# http://www.uq.edu.au
#
# Development from 2018 by Departamento de Engenharia de Computação e Automação
#
# Primary Business: Rio Grande do Norte, Brazil
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
##############################################################################

export OMP_NUM_THREADS=4
export OMP_PROC_BIND=TRUE
export OMP_SCHEDULE=guided

echo 'Defining LD_LIBRARY_PATH and PYTHONPATH variables for the Shirley module...'

# Contains "libboost_python38.so" or similar
BOOST_LIB_PATH="/usr/lib/x86_64-linux-gnu"
ESCRIPT_LIB_PATH="/path/to/escript-5.7/lib"
SHIRLEY_LIB_PATH="/path/to/shirley/lib/"

ESCRIPT_PATH="/path/to/escript-5.7"
SHIRLEY_PATH="/path/to/shirley"

export LD_LIBRARY_PATH=${BOOST_LIB_PATH}:${ESCRIPT_LIB_PATH}:${SHIRLEY_LIB_PATH}:${LD_LIBRARY_PATH}
export PYTHONPATH=${ESCRIPT_PATH}:${SHIRLEY_PATH}:${PYTHONPATH}

set -x

THR_COUNT=$OMP_NUM_THREADS
ORDER=5

VTUNE_TEMP_DIR="${HOME}/temp/shirley-bench-1/vtune-temp-dir"
VTUNE_OUTPUT_DIR="vtune-output/thr-${THR_COUNT}"

mkdir -p ${VTUNE_TEMP_DIR}
mkdir -p ${VTUNE_OUTPUT_DIR}

for SIZE in {0..2}
do
  for STRATEGY in {0..3}
  do
    echo '' > ${VTUNE_OUTPUT_DIR}/exec-sim-${SIZE}-${STRATEGY}-${ORDER}.txt
    echo '' > ${VTUNE_OUTPUT_DIR}/exec-rpt-${SIZE}-${STRATEGY}-${ORDER}.csv

    for IDX in {1..5}
    do
      vtune -collect memory-access -knob sampling-interval=3 -data-limit=20480 \
        -r ${VTUNE_TEMP_DIR} \
        -- ./run-vtune-single.sh ${THR_COUNT} ${SIZE} ${STRATEGY} ${ORDER}
      vtune -report summary -report-knob show-issues=false \
        -r ${VTUNE_TEMP_DIR} -format=csv \
        >> ${VTUNE_OUTPUT_DIR}/exec-rpt-${SIZE}-${STRATEGY}-${ORDER}.csv
      rm -rf ${VTUNE_TEMP_DIR}
    done
  done
done

rm -rf ${VTUNE_TEMP_DIR}
