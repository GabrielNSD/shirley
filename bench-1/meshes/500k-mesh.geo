// Taken from:
//   https://fenicsproject.discourse.group/t/converting-simple-2d-mesh-from-gmsh-to-dolfin/583/3

// Increasing the value of Mesh.CharacteristicLengthFactor results in a
// coarser mesh.
// See https://scicomp.stackexchange.com/a/31043
Mesh.CharacteristicLengthFactor = 0.01455;

//Mesh.Algorithm=2;
//Mesh.Algorithm3D=4;

// The square
Point(1) = {0, 0, 0};
Point(2) = {0, 1000, 0};
Point(3) = {2000, 1000, 0};
Point(4) = {2000, 0, 0};

Line(1) = {1, 4};
Line(2) = {4, 3};
Line(3) = {3, 2};
Line(4) = {2, 1};

// Create the surfaces
Line Loop(6) = {2, 3, 4, 1};
Plane Surface(8) = {6};

Physical Surface(2) = {8};
//+
