#!/bin/bash

##############################################################################
#
# esys-escript:
# Copyright (c) 2003-2021 by The University of Queensland
# http://www.uq.edu.au
#
# Primary Business: Queensland, Australia
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
# Development until 2012 by Earth Systems Science Computational Center (ESSCC)
# Development 2012-2013 by School of Earth Sciences
# Development from 2014 by Centre for Geoscience Computing (GeoComp)
# Development from 2019 by School of Earth and Environmental Sciences
#
# Shirley domain module:
# Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
# and The University of Queensland
# https://www.ufrn.br
# http://www.uq.edu.au
#
# Development from 2018 by Departamento de Engenharia de Computação e Automação
#
# Primary Business: Rio Grande do Norte, Brazil
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
##############################################################################

# $1 = thread count
THR_COUNT=$1
if [ -z "${THR_COUNT}" ]
then
  THR_COUNT=4
fi

# $2 = mesh size (0=250k elements, 1=500k elements, 2=1M elements)
SIZE=$2
if [ -z "${SIZE}" ]
then
  SIZE=1
fi

# $3 = traversal strategy (0=connectivity, 1=distance, 2=Hilbert curves; any other value for "none")
STRATEGY=$3
if [ -z "${STRATEGY}" ]
then
  STRATEGY=2
fi

# $4 = polynomial order (5, 6, 7)
ORDER=$4
if [ -z "${ORDER}" ]
then
  ORDER=5
fi

time python3 regular-sim.py ${SIZE} ${STRATEGY} ${ORDER} \
  >> regular-output/thr-${THR_COUNT}/exec-sim-${SIZE}-${STRATEGY}-${ORDER}.txt
