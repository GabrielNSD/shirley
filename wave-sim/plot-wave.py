
##############################################################################
#
# esys-escript:
# Copyright (c) 2003-2021 by The University of Queensland
# http://www.uq.edu.au
#
# Primary Business: Queensland, Australia
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
# Development until 2012 by Earth Systems Science Computational Center (ESSCC)
# Development 2012-2013 by School of Earth Sciences
# Development from 2014 by Centre for Geoscience Computing (GeoComp)
# Development from 2019 by School of Earth and Environmental Sciences
#
# Shirley domain module:
# Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
# and The University of Queensland
# https://www.ufrn.br
# http://www.uq.edu.au
#
# Development from 2018 by Departamento de Engenharia de Computação e Automação
#
# Primary Business: Rio Grande do Norte, Brazil
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
##############################################################################

import numpy as np

import matplotlib.pylab as plt
import matplotlib.cm as cm
from scipy.interpolate import griddata

num_files = 81

in_dir = "/home/username/Temp/wave-sim"
file_u = in_dir + "/u%s.csv"

out_dir = "/home/username/Temp/wave-sim"
image_u = out_dir + "/u%s.png"

x_segments=2000
y_segments=1000

ampl = 5.5

for k in range(num_files):
    print('k=', k)

    # Plot the wave profile
    u = np.loadtxt(file_u % k, delimiter=', ', skiprows=1)
    plt.figure(figsize=(9.0 * (348.0 / 281.0), 4.5))

    X = np.linspace(u[:, 1].min(), u[:, 1].max(), x_segments)
    Y = np.linspace(u[:, 2].min(), u[:, 2].max(), y_segments)
    U, V = np.meshgrid(X, Y)
    print('U ...:', U.min(), '/', U.max())
    print('V ...:', V.min(), '/', V.max())

    z = u[:, 0]
    print('z ...:', z.min(), '/', z.max())
    gdata = griddata(u[:, 1:3], u[:, 0], (U, V),
                     method='linear', fill_value=0.) * ampl
    gdata_min, gdata_max = gdata.min(), gdata.max()
    gdata_range = gdata_max - gdata_min
    print('gdata:', gdata_min, '/', gdata_max, '/range:', gdata_range)

    vmin = -22300.0 * ampl
    vmax = +22300.0 * ampl
    levels = 100 if gdata_min == gdata_max else np.linspace(vmin, vmax, 100)
    c = plt.contourf(U, V, gdata, levels, cmap='seismic',
                     vmin=vmin, vmax=vmax, extend='both')

    plt.colorbar(c)
    plt.savefig(image_u % k)
    print(image_u % k, " processed.")
