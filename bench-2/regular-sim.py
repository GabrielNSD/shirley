
##############################################################################
#
# esys-escript:
# Copyright (c) 2003-2021 by The University of Queensland
# http://www.uq.edu.au
#
# Primary Business: Queensland, Australia
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
# Development until 2012 by Earth Systems Science Computational Center (ESSCC)
# Development 2012-2013 by School of Earth Sciences
# Development from 2014 by Centre for Geoscience Computing (GeoComp)
# Development from 2019 by School of Earth and Environmental Sciences
#
# Shirley domain module:
# Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
# and The University of Queensland
# https://www.ufrn.br
# http://www.uq.edu.au
#
# Development from 2018 by Departamento de Engenharia de Computação e Automação
#
# Primary Business: Rio Grande do Norte, Brazil
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
##############################################################################

import sys
import time

from esys.escript import *
from esys.speckley import Rectangle
from esys.escript.pdetools import Locator
import numpy as np

from shirley.shirleycpp import ReadGmsh

print('Usage: python3 regular-sim.py <mesh size=1> <traversal strategy=2> <order=5>')
print('Domain sizes: 0 = 250k elements')
print('              1 = 500k elements')
print('              2 = 1M elements')
print('(Anything else) = 500k elements')
print('Traversal strategies: 0 = Connectivity-based')
print('                      1 = Distance-based')
print('                      2 = Hilbert curves')
print('        (Anything else) = No strategy')
print('Polynomial orders: 4,5,6,7')

# Set mesh size
mesh_size = 1

if len(sys.argv) > 1:
    mesh_size = int(sys.argv[1])
    if (mesh_size < 0) or (mesh_size > 2): mesh_size = 1

print('Mesh size=', mesh_size)

# Set traversal strategy
strategy = 2

if len(sys.argv) > 2:
    strategy = int(sys.argv[2])

print('Strategy=', strategy)

# Set polynomial order
order = 5

if len(sys.argv) > 3:
    order = int(sys.argv[3])
    if (order < 4) or (order > 7): order = 5

print('Polynomial order=', order)

# Load the mesh
load_start_time = time.perf_counter()

if mesh_size == 0:
    mesh_file = "meshes/250k-horizon.msh"
if mesh_size == 1:
    mesh_file = "meshes/500k-horizon.msh"
if mesh_size == 2:
    mesh_file = "meshes/1m-horizon.msh"

dom = ReadGmsh(filename=mesh_file,
               numDim=2, order=order, traversalStrategy=strategy)
load_end_time = time.perf_counter()

print('Mesh loading time (C++) =', load_end_time - load_start_time, 'seconds')

# Set up the wave
dx = 40
nx = 32  # Should be even
nz = nx
# For Speckley
lx = nx * dx
lz = nz * dx

# This is a case of a traveling wave 'hump' u=f(d.x-c*t),
# where vector d is the direction of travel (|d|=1).
# Then we have v=-d/c*f(d.x-c*t).
c = 2000.  # Wave velocity in m/s
d = [1., 0.]  # Or [0.,1.] - other directions will not work due to boundary conditions
#
#  f( s ) =( (1-(s-s0)/w)**2 )**pro_m for |s-s0|/w<1
#         = 0                      otherwise
#
# Half width of the hump - this should not be too small
w = 15 * dx
# Offset of the hump center from 0
s0 = 16 * dx
pro_m = 4
assert s0 - w > 0, "profile"
assert s0 + w < lx, "profile"

def getProfile(x, t):
    a = (x[0] * d[0] + x[1] * d[1] - s0 - c * t) / w
    f = (1 - clip(a, minval=-1., maxval=1.) ** 2) ** pro_m
    return f

# Terms for the initial solution
rho = 1.  # Density of the medium in kg/m^3
u0 = getProfile(Solution(dom).getX(), t=0)
p = getProfile(Function(dom).getX(), t=0)
v0 = -p / c * d

print("v0 =", v0)
print("u0 =", u0)

# Set up coefficients
m_M = Scalar(1. / (rho * c**2), Function(dom), expanded=True)
m_F = Tensor((1. / rho) * kronecker(dom.getDim()), Function(dom), expanded=True)
m_B = Tensor(-1. * kronecker(dom.getDim()), Function(dom), expanded=True)
dom.setPdeCoefficients(F=m_F, M=m_M, B=m_B)

# Set up initial solution
dom.setInitialSolution(u=u0, v=v0, currentTime=0.)

# We position recorders along the line of travel
# as phone is positioned every DPhones grid line:
DPhones = 4

if abs(d[0]) > 0:
    recorderPositions = [(dx * i, dx * (nz // 2)) for i in range(2, nx - 2, DPhones)]
else:
    recorderPositions = [(dx * (nx // 2), dx * i) for i in range(2, nz - 2, DPhones)]

loc = Locator(Solution(dom), recorderPositions)
print(len(recorderPositions), "geophones")
print("Recorder positions= ", loc.getX())

# For the time integration loop
timesteps = []
records = []

dt_adjust = 1.

if mesh_size == 0:
    dt_adjust = (250550.0 / 250550.0) * 10.0  # 250k elements
if mesh_size == 1:
    dt_adjust = (500030.0 / 250550.0) * 15.0  # 500k elements
if mesh_size == 2:
    dt_adjust = (1000442.0 / 250550.0) * 30.0  # 1M elements

dt = 0.0001 / dt_adjust
dom.calculateTimeIntegrationMatrices(timeStep=dt)
steps_per_iter = int(0.0001 / dt_adjust)
print('Steps per iteration=', steps_per_iter)

t = 0.
u = dom.getU()
v = dom.getV()

total_cpu_time = 0
total_steps = 0

for k in range(6):
    if k > 0:
        start_time = time.perf_counter()
        dom.progressInSteps(steps=steps_per_iter)
        end_time = time.perf_counter()
        elapsed_time = float(end_time - start_time)
        print('Elapsed clock time=', elapsed_time, 's')

        total_cpu_time += elapsed_time
        total_steps += steps_per_iter

        t = dom.getCurrentTime()
        u = dom.getU()
        v = dom.getV()

        print('Current simulation time=', t)

    print("u = ", u)
    print("v[0] = ", v[0])
    print("v[1] = ", v[1])

    timesteps.append(dt * k * steps_per_iter)
    records.append(loc(u))

    print('k =', k, ' / final time =', dt * k * steps_per_iter)

print("Last trace @ ", timesteps[-1], records[-1])

print("Total clock time=", total_cpu_time, 's')
print("Total steps=", total_steps)
print("Clock time per step=", total_cpu_time / total_steps, 's')
