// Taken from:
//   https://fenicsproject.discourse.group/t/converting-simple-2d-mesh-from-gmsh-to-dolfin/583/3

// Increasing the value of Mesh.CharacteristicLengthFactor results in a
// coarser mesh. See:
//   https://scicomp.stackexchange.com/a/31043
Mesh.CharacteristicLengthFactor = 0.2015;

// Square
Point(1) = {0, 0, 0};
Point(2) = {0, 1280, 0};
Point(3) = {1280, 1280, 0};
Point(4) = {1280, 0, 0};

Line(1) = {1, 4};
Line(2) = {4, 3};
Line(3) = {3, 2};
Line(4) = {2, 1};

// Surfaces
Line Loop(6) = {2, 3, 4, 1};
Plane Surface(8) = {6};

Physical Surface(2) = {8};
