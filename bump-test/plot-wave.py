
##############################################################################
#
# esys-escript:
# Copyright (c) 2003-2021 by The University of Queensland
# http://www.uq.edu.au
#
# Primary Business: Queensland, Australia
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
# Development until 2012 by Earth Systems Science Computational Center (ESSCC)
# Development 2012-2013 by School of Earth Sciences
# Development from 2014 by Centre for Geoscience Computing (GeoComp)
# Development from 2019 by School of Earth and Environmental Sciences
#
# Shirley domain module:
# Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
# and The University of Queensland
# https://www.ufrn.br
# http://www.uq.edu.au
#
# Development from 2018 by Departamento de Engenharia de Computação e Automação
#
# Primary Business: Rio Grande do Norte, Brazil
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
##############################################################################

import numpy as np
import matplotlib.pylab as plt
import matplotlib.cm as cm
from scipy.interpolate import griddata

FileU = "output/u%s.csv"
FileV = "output/v%s.csv"

ImageU = "output/u%s.png"
ImageV = "output/v%s.png"

NFiles = 31
NQuiver = 40  # Used in interpolation, allows for crisper images

for k in range(NFiles):
    # Plot wavefield u
    u = np.loadtxt(FileU%k, delimiter=', ', skiprows=1)
    plt.figure()

    X = np.linspace(u[:,1].min(), u[:,1].max(), NQuiver)
    Y = np.linspace(u[:,2].min(), u[:,2].max(), NQuiver)
    U, V = np.meshgrid(X, Y)
    gdata = griddata(u[:,1:3], u[:,0], (U, V), method='linear')
    c = plt.contourf(U, V, gdata, 50, cmap='jet', vmin=0.0, vmax=1.0)

    plt.colorbar(c)
    plt.savefig(ImageU%k)
    print(ImageU%k, "processed.")
    
    # Plot wavefield v
    v = np.loadtxt(FileV%k, delimiter=', ', skiprows=1)
    X = np.linspace(v[:,2].min(), v[:,2].max(), NQuiver)
    Y = np.linspace(v[:,3].min(), v[:,3].max(), NQuiver)
    U, V = np.meshgrid(X, Y)
    gdata = griddata(v[:,2:4], v[:,0:2], (U, V), method='linear')
    plt.figure()
    plt.quiver(U, V, gdata[:,:,0], gdata[:,:,1])
    plt.savefig(ImageV%k)
    print(ImageV%k, "processed.")
